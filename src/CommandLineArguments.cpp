// CommandLineArguments.cpp is part of Dilute-Dense Solver
// Copyright (C) 2019 M. Mace

#ifndef __COMMANDLINEARGUMENTS_CPP__
#define __COMMANDLINEARGUMENTS_CPP__

///////////////////////////////////
// PROCESS COMMANDLINE ARGUMENTS //
///////////////////////////////////

void ProcessCommandlineArguments(int argc,char **argv){

    Konfig arguments(argc,argv);

    // SET OUTPUT DIRECTORY //
    char OutDir[512]="OUTPUT";

    arguments.Getval("o",OutDir);

    IO::SetOutputDirectory(OutDir);

    std::cerr << "## OUTPUT DIRECTORY IS " << IO::OutputDirectory <<  std::endl;

    INT NSites=-1;

    arguments.Getval("N",NSites);

    if(NSites>0){
        Lattice::N[0]=NSites;
        Lattice::N[1]=NSites;
        Lattice::Area=NSites*NSites;
        std::cerr << "## LATTICE TRANSVERSE SIZE IS " << Lattice::N[0] << "x" << Lattice::N[1] << std::endl;
    }
    else{
        std::cerr << "## NUMBER OF SITES NOT SPECIFIED -- USING " << Lattice::N[0] << "x" << Lattice::N[1] << std::endl;
    }

    DOUBLE aSpacing=-1.0;

    arguments.Getval("a",aSpacing);

    DOUBLE aXSpacing=aSpacing;
    DOUBLE aYSpacing=aSpacing;

    arguments.Getval("ax",aXSpacing);
    arguments.Getval("ay",aYSpacing);

    if((aXSpacing>0.0)&&(aYSpacing>0.0)){

        Lattice::a[0]=aXSpacing;
        Lattice::a[1]=aYSpacing;

        std::cerr << "## LATTICE SPACINGS ARE ax=" << Lattice::a[0] << " ay=" << Lattice::a[1] <<  std::endl;
    }
    else{
        std::cerr << "## LATTICE SPACING NOT SPECIFIED -- USING " << Lattice::a[0] << " " << Lattice::a[1] <<  std::endl;
    }

    Lattice::SizeX=Lattice::a[0]*Lattice::N[0];
    Lattice::SizeY=Lattice::a[1]*Lattice::N[1];

    std::cerr << "## SPATIAL EXTENT IS " << Lattice::SizeX << " x " << Lattice::SizeY << std::endl;

    INT NSitesRap=-1;

    arguments.Getval("NRap",NSitesRap);

    if(NSitesRap>0){
        Lattice::NRap=NSitesRap;
        std::cerr << "## NUMBER OF SLICES IN RAPIDITY=" << Lattice::NRap << std::endl;
    }
    else{
        std::cerr << "## NUMBER OF SLICES IN RAPIDITY NOT SPECIFIED -- USING " << Lattice::NRap << std::endl;
    }

    arguments.Getval("RegMass",RegMass);
    std::cerr << "## RegMass=" << RegMass << std::endl;
    
    arguments.Getval("kMin",kMin);
    arguments.Getval("kMax",kMax);
    arguments.Getval("kREFMin",kREFMin);
    arguments.Getval("kREFMax",kREFMax);
    arguments.Getval("dkBin",dkBin);
    arguments.Getval("dkStep",dkStep);
    arguments.Getval("kINT_FLAG",kINT_FLAG);

    std::cerr << "## kMin=" << kMin << " kMax=" << kMax << " kREFMin=" << kREFMin << " kREFMax=" << kREFMax << " dkStep=" << dkStep << " dkBin=" << dkBin << " kINT_FLAG=" << kINT_FLAG << std::endl;

    arguments.Getval("AS",AS_FLAG);

    std::cerr << "## MEASUREMENT OF ANTISYMM=" << AS_FLAG << " (0 -- OFF, 1 -- ON)" << std::endl;

    // GIPS SPECIFIC ARGUEMNTS //
    #if IC_FLAG==GIPS_FLAG
    arguments.Getval("OUT",OUTPUT_FLAG);
    std::cerr << "## OUTPUT OF CONFIGURATIONS=" << OUTPUT_FLAG << " (0 -- OFF, 1 -- ON)" << std::endl;
    
    char InFile[512]="input";
    arguments.Getval("if",InFile);
    IO::SetInputFile(InFile);
    std::cerr << "## INPUT FILE IS " << IO::InputFile <<  std::endl;
    #endif
    
    // MV SPECIFIC ARGUEMNTS //
    
    #if IC_FLAG==MV_FLAG
    OUTPUT_FLAG=0;

    // SET SATURATION SCALES //
    arguments.Getval("g2muP",g2muP);
    arguments.Getval("g2muT",g2muT);

    std::cerr << "## g2muP=" << g2muP  << " g2muT=" << g2muT << std::endl;
    #endif
}
#endif
