#ifndef __MOMENTA_CD__CPP__
#define __MOMENTA_CD__CPP__

#define GetMomentaCD(pXIndex,pYIndex,pXValue,pYValue) \
\
pXValue=sin(DOUBLE(2.0)*PI*pXIndex/Lattice::N[0])/TargetFields::U->a[0]; \
pYValue=sin(DOUBLE(2.0)*PI*pYIndex/Lattice::N[1])/TargetFields::U->a[1];

#define GetAbsMomentumCD(pXIndex,pYIndex,pXValue,pYValue,pAbs)\
\
pXValue=sin(DOUBLE(2.0)*PI*pXIndex/Lattice::N[0])/TargetFields::U->a[0]; \
pYValue=sin(DOUBLE(2.0)*PI*pYIndex/Lattice::N[1])/TargetFields::U->a[1]; \
\
pAbs=sqrt(SQR(DOUBLE(2.0)*sin(PI*pXIndex/Lattice::N[0])/TargetFields::U->a[0])+SQR(DOUBLE(2.0)*sin(PI*pYIndex/Lattice::N[1])/TargetFields::U->a[1]));

#define GetAbsMomentumOnlyCD(pXIndex,pYIndex,pAbs)\
\
pAbs=sqrt(SQR(DOUBLE(2.0)*sin(PI*pXIndex/Lattice::N[0])/TargetFields::U->a[0])+SQR(DOUBLE(2.0)*sin(PI*pYIndex/Lattice::N[1])/TargetFields::U->a[1]));

#endif

