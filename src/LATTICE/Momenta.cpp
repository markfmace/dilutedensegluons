#ifndef __MOMENTA__CPP__
#define __MOMENTA__CPP__

#define GetMomenta(pXIndex,pYIndex,cDpx,cDpy) \
\
cDpx=-ComplexI*(DOUBLE(1.0)-exp(-ComplexI*DOUBLE(2.0)*PI*DOUBLE(pXIndex)/DOUBLE(TargetFields::U->N[0])))/TargetFields::U->a[0]; \
cDpy=-ComplexI*(DOUBLE(1.0)-exp(-ComplexI*DOUBLE(2.0)*PI*DOUBLE(pYIndex)/DOUBLE(TargetFields::U->N[1])))/TargetFields::U->a[1];

#define GetAbsMomentum(pXIndex,pYIndex,cDpx,cDpy,pAbs)\
\
cDpx=-ComplexI*(DOUBLE(1.0)-exp(-ComplexI*DOUBLE(2.0)*PI*DOUBLE(pXIndex)/DOUBLE(TargetFields::U->N[0])))/TargetFields::U->a[0]; \
cDpy=-ComplexI*(DOUBLE(1.0)-exp(-ComplexI*DOUBLE(2.0)*PI*DOUBLE(pYIndex)/DOUBLE(TargetFields::U->N[1])))/TargetFields::U->a[1]; \
\
pAbs=sqrt(SQR_ABS(cDpx)+SQR_ABS(cDpy));

#endif

