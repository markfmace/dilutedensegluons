
//COMPUTE THE DETERMINANT OF A COMPLEX 3x3 MATRIX
COMPLEX GeneralDet(COMPLEX A[3][3]){
    return A[0][2]*(A[1][0]*A[2][1]-A[1][1]*A[2][0])+A[0][1]*(A[1][2]*A[2][0] -A[1][0]*A[2][2])+A[0][0]*(A[1][1]*A[2][2]-A[1][2]*A[2][1]);
}

//COMPUTE THE INVERSE OF A COMPLEX 3x3 MATRIX
void GeneralInverse(COMPLEX A[3][3],COMPLEX AInv[3][3]){
    
    COMPLEX OneOverDetA=DOUBLE(1.0)/GeneralDet(A);
    
    AInv[0][0]=(A[1][1]*A[2][2]-A[1][2]*A[2][1])*OneOverDetA;
    AInv[0][1]=(A[0][2]*A[2][1]-A[0][1]*A[2][2])*OneOverDetA;
    AInv[0][2]=(A[0][1]*A[1][2]-A[0][2]*A[1][1])*OneOverDetA;
    
    AInv[1][0]=(A[1][2]*A[2][0]-A[1][0]*A[2][2])*OneOverDetA;
    AInv[1][1]=(A[0][0]*A[2][2]-A[0][2]*A[2][0])*OneOverDetA;
    AInv[1][2]=(A[0][2]*A[1][0]-A[0][0]*A[1][2])*OneOverDetA;
    
    AInv[2][0]=(A[1][0]*A[2][1]-A[1][1]*A[2][0])*OneOverDetA;
    AInv[2][1]=(A[0][1]*A[2][0]-A[0][0]*A[2][1])*OneOverDetA;
    AInv[2][2]=(A[0][0]*A[1][1]-A[0][1]*A[1][0])*OneOverDetA;
}

