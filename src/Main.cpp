////////////////////////////////////////////////////////////////////////////////////////
// PROGRAM TO COMPUTE PARTICLE PRODUCTION FOR NUCLEAR COLLISONS IN DILUTE DENSE LIMIT //
////////////////////////////////////////////////////////////////////////////////////////

#define IC_FLAG GIPS_FLAG

#include <iostream>
#include <string>

// MOST BASIC DEFINITIONS //
#include "Definitions.cpp"

// INCLUDE MPI SAMPLING //
#include "MPI/Basic.cpp"

// SET GAUGE GROUP //
#define SU_Nc_FLAG SU3_FLAG

/////////////////////////////////////////
// SU(Nc) GROUP AND ALGEBRA OPERATIONS //
/////////////////////////////////////////

// INCLUDE SU(Nc) ALGEBRA AND GROUP DEFINITION //
#include "SUNc/Definitions.cpp"

// INCLUDE GSL INTERPOLATION AND INTEGRATION ROUTINES //
#include "MISC/Interpolation.cpp"
#include "MISC/Integration.cpp"

// DEFINE 3D GRID //
#include "LATTICE/2DGrid.cpp"

// DEFINE WILSON LINE CLASS //
#include "LATTICE/WilsonLines.cpp"
// DEFINE VECTOR FIELDS CLASS //
#include "LATTICE/VectorFields.cpp"
#include "LATTICE/ComplexVectorFields.cpp"

// DEFINE GAUGE LINK AND ELECTRIC FIELD VARIABLES //
#include "LATTICE/Fields.cpp"

// HISTOGRAM //
#include "MISC/HISTOGRAM/Histogram.cpp"
#include "MISC/HISTOGRAM/MultiHistogram.cpp"

///////////////////////////
// SIMULATION PARAMETERS //
///////////////////////////

// DEFINE GLOBAL IMPACT PARAMETER //
DOUBLE bImpact; // [fm] //

DOUBLE RegMass=0.3; // [GeV] //

// MOMENTUM VARIABLES //
DOUBLE kMin=0.5;//3.0/2.0*4.0*PI/(Lattice::SizeX);
DOUBLE kMax=3.5;//2.0*PI/(Lattice::SizeX);
DOUBLE dkBin=0.5; // MOMENTUM INTEGRATION bIN
DOUBLE dkStep=0.5; // STEP IN MOMENTUM
DOUBLE kREFMin=0.5; // FOR REFERENCE BIN //
DOUBLE kREFMax=3.0; // FOR REFERENCE BIN  //
INT kINT_FLAG=1; // 0 -- \phi kd k / 1 -- \phi k / 2 -- \phi //

INT OUTPUT_FLAG=0;

INT AS_FLAG=0;

// DO NOT CHANGE -- SET IN COMMANDLINE//
#if IC_FLAG==MV_FLAG
DOUBLE g2muP=0.5; DOUBLE g2muT=1.0;
#endif

// DEFINE RANDOM NUMBER GENERATOR //
#include "MISC/RNG/GSLRandomNumberGenerator.cpp"

///////////////////////////
//  OUTPUT HANDLING      //
///////////////////////////

#include "IO/StringManipulation.cpp"
#include "IO/OutputManagement.cpp"
#include "IO/InputFileManagement.cpp"
#include "IO/SaveConfiguration.cpp"

// LOAD //
#include "IO/He3Configurations.cpp"

// GLOBAL RANDOM GENERATOR SEED //
INT GLOBAL_RNG_SEED;
// MPI RANDOM NUMBER SEED //
INT MY_MPI_RNG_SEED;

// INCLUDE IMPACT PARAMETER SAMPLING //
#include "INITIALCONDITIONS/DetermineChargesAndFields.cpp"

// INCLUDE COMMANDLINE PROCESSING //
INT NumberOfConfigurations=1;
#include "IO/cfile.c"
#include "CommandLineArguments.cpp"

///////////////////////////
//     OBSERVABLES       //
///////////////////////////

// INCLUDE OBSERVABLES
#include "OBSERVABLES/SingleParticleDistribution.cpp"

// OUTPUT DIRECTORY //
std::string OutputDirectory;

// CREATE INFO FILE //
#include "CreateInfoFile.cpp"

// FFTW TEST
#include "TESTS/TestFFTW.cpp"

void Run(int argc,char **argv,INT MPI_RNG_SEED){

    ///////////
    // SETUP //
    ///////////

    MY_MPI_RNG_SEED=MPI_RNG_SEED;

    std::cerr << "## SEED=" << MY_MPI_RNG_SEED << std::endl;
    
    //INITIALIZE RANDOM NUMBER GENERATOR //
    RandomNumberGenerator::Init(MY_MPI_RNG_SEED);

    // INITIALIZE SET GROUP ELEMENTS TO UNITY AND ALGEBRA ELEMENTS TO ZERO //
    Lattice::InitTargetAndProjFields(); ProjSolution::A->SetZero();
    TargetFields::U->SetIdentity(); TempTargetFields::U->SetIdentity();
    Lattice::Initg2mu(); g2mu::Targ->SetZero();  g2mu::Proj->SetZero();

    // INITIALIZE RHO FOURIER SPACE //
    FourierSpace::InitRhos();
    
    // RUN FFTW TEST //
    //TestFFTW(TargetFields::U);
    //exit(0);

    // SET INITIAL FIELDS //
    InitialConditions::DetermineChargesAndFields(MPI_RNG_SEED);
    
    // CLEANUP Tpp, g2mu FEILDS, AND RHO FIELDS //
    Lattice::CleanUpg2mu(); FourierSpace::CleanUpRhos();

    // INITIALIZE OMEGA LATTICE FIELDS, OMEGA AND SCALAR FOURIER SPACE //
    Lattice::InitOmegas();  OmegaS::O->SetZero();   OmegaA::O->SetZero();
    FourierSpace::InitOmegas(); FourierSpace::InitScalar();

    // COMPUTE SINGLE PARTICLE DISTRIBITON FOR A GIVEN SET OF RHOS //
    Observables::ComputeSingleParticleDistribtion();

    // CLEAN-UP TARGET AND PROJECTILE FIELDS, OMEGAS, SCALAR //
    Lattice::CleanUpTargetAndProjFields(); Lattice::CleanUpOmegas();
    FourierSpace::CleanUpOmegas(); FourierSpace::CleanUpScalar();

    //Lattice::CleanUp();
    RandomNumberGenerator::CleanUp();

}


int main(int argc,char **argv){

    ///////////////////////////////
    //INITIALIZE MPI ENVIRONMENT //
    ///////////////////////////////

    MPI_Init(&argc, &argv);
    MPI_Barrier(MPI_COMM_WORLD);

    //SET MPI ID's AND NUMBER OF NODES

    MPI_Comm_rank(MPI_COMM_WORLD,&MPIBasic::ID);
    MPI_Comm_size(MPI_COMM_WORLD,&MPIBasic::NumberOfNodes);

    //////////////////////////////////
    //PROCESS COMMANDLINE ARGUMENTS //
    //////////////////////////////////
    ProcessCommandlineArguments(argc,argv);


    ////////////////////////////////////
    // INITIALIZE OPEN MP ENVIRONMENT //
    ////////////////////////////////////

    //INITIALIZE THREADED FFTW
    int FFTW3_THREAD_STATUS=fftw_init_threads();
    
    //CHECK THREAD COUNT
    std::cerr << "# NUMBER OF THREADS= " << omp_get_max_threads() << " FFTW THREAD STATUS= " << FFTW3_THREAD_STATUS << std::endl;

    if(FFTW3_THREAD_STATUS==1){

        fftw_plan_with_nthreads(omp_get_max_threads());

    }
    
    //////////////////////////////////
    //PROCESS COMMANDLINE ARGUMENTS //
    //////////////////////////////////

    Konfig arguments(argc,argv);

    // NUMBER OF CONFIGURATIONS RUN IN SERIAL //
    arguments.Getval("nconfs",NumberOfConfigurations);

    std::cerr << "# RUNNING " << NumberOfConfigurations << " CONFIGURATIONS " << std::endl;

    // SAMPLE DIFFERENT CONFIGURATIONS //
    for(INT n=0;n<NumberOfConfigurations;n++){

        //SET GLOBAL RANDOM NUMBER SEED//
        INT GLOBAL_RNG_SEED;

        if(MPIBasic::ID==0){

            GLOBAL_RNG_SEED=time(0);
            
            arguments.Getval("SEED",GLOBAL_RNG_SEED);
        }

        // BROADCAST GLOBAL RANDOM SEED //
        MPI_Bcast(&GLOBAL_RNG_SEED, 1, MPI_INT,0,MPI_COMM_WORLD);
        
        // CREATE INFO FILE //
        CreateInfoFile(GLOBAL_RNG_SEED+MPIBasic::ID);
        std::cerr << "# INFO FILE CREATED " << std::endl;

        // PERFORM CLASSICAL STATISTICAL SIMULATION //
        Run(argc,argv,GLOBAL_RNG_SEED+MPIBasic::ID);

        // COMMADNLINE NOTIFICATION //
        std::cerr << "## COMPLETED " << GLOBAL_RNG_SEED+MPIBasic::ID << std::endl;

    }

    //SYNCHRONIZE ALL MPI NODES
    MPI_Barrier(MPI_COMM_WORLD);

    //FINALIZE MPI
    MPI_Finalize();

    //////////
    // EXIT //
    //////////

    exit(0);

}
