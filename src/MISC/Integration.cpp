#ifndef __INTEGRATION_CPP__
#define __INTEGRATION_CPP__

#include <gsl/gsl_integration.h>

// PERFORMS NUMERICAL GAUSS KONRAD INTEGRATION BETWEEN TWO FINITE BOUNDS //
DOUBLE GKIntegration(DOUBLE (*f)(DOUBLE,void *),void *params,DOUBLE a,DOUBLE b,DOUBLE eps){
    
    // f: function to integrate. It must take two arguments: the first
    // one is the integration variable, and the second one is a pointer
    // to some extra parameters (it can be a table of parameters, or a
    // structure).
    // params: a pointer to the extra parameters to pass to the function f
    // a: the lower integration limit
    // b: the upper integration limit
    // eps: a parameter that controls the precision of the integration
    
    gsl_integration_workspace *wksp=gsl_integration_workspace_alloc(300);
    DOUBLE result,abserr;
    DOUBLE epsabs=eps;
    DOUBLE epsrel=eps;
    gsl_function F;
    
    F.function=f;
    F.params=params;
    
    gsl_integration_qag(&F,a,b,epsabs,epsrel,300,6,wksp,&result,&abserr);
    gsl_integration_workspace_free(wksp);
    return result;
}

#endif
