#ifndef __INTERPOLATION_CPP__
#define __INTERPOLATION_CPP__

#include <gsl/gsl_interp.h>
#include <gsl/gsl_spline.h>

DOUBLE interpolation_debug(DOUBLE v_, gsl_spline* spl_){
    
    INT iv_;
    for(iv_=0;(unsigned)iv_<(spl_->size);iv_++){
        if(v_>(spl_->x)[iv_] && v_<(spl_->x)[iv_+1]){
            break;
        }
    }
    
    DOUBLE del_x=(spl_->x)[iv_]-(spl_->x)[iv_+1];
    DOUBLE del_y=(spl_->y)[iv_]-(spl_->y)[iv_+1];
    DOUBLE x_lin=v_-(spl_->x)[iv_];
    DOUBLE y_lin=(spl_->y)[iv_]+(x_lin*del_y/del_x);
    

    return y_lin;
}

DOUBLE var_interpolated(DOUBLE v_, gsl_spline* spl_){
    
    if(fabs(v_-(spl_->x)[(spl_->size)-1])<1e-10){
        
        std::cerr << "### WARNING : Interpolation x=" << v_ << " value marginal (" << (spl_->x)[0] <<  "-" << (spl_->x)[(spl_->size)-1] << ")" << std::endl;;

    }
    else if(v_<(spl_->x)[0] || v_>(spl_->x)[(spl_->size)-1]){
        return 0.0;
    }
    
    gsl_interp_accel *acc=gsl_interp_accel_alloc();
    
    double var_i=gsl_spline_eval(spl_,v_,acc);
    
    if(var_i<0){
        var_i=interpolation_debug(v_,spl_);
    }
    
    gsl_interp_accel_free(acc);
        
    return var_i;
}

// OVERLOAD -- 'order' DENOTES ORDER OF DERIVATIES, NOT INTERPOLATION //
DOUBLE var_interpolated(DOUBLE v_,gsl_spline* spl_,INT order){
    
    if(fabs(v_-(spl_->x)[(spl_->size)-1])<1e-10){

        std::cerr << "### WARNING : Interpolation x=" << v_ << " value marginal (" << (spl_->x)[0] << "-" << (spl_->x)[(spl_->size)-1] << ")" << std::endl;;

    }
    else if(v_<(spl_->x)[0] || v_>=(spl_->x)[(spl_->size)-1]){
        
        std::cerr << "### ERROR : Interpolation x=" << v_ << " value out of range (" << (spl_->x)[0] << "-" << (spl_->x)[(spl_->size)-1] << ")" << std::endl;;

        return 0.0;
    }
    
    gsl_interp_accel *acc=gsl_interp_accel_alloc();
    
    double var_i=gsl_spline_eval(spl_,v_,acc);
    
    if(order==0){
        return var_i;
    }else if(order==1){
        var_i=gsl_spline_eval_deriv(spl_,v_,acc);
    }else if(order==2){
        var_i=gsl_spline_eval_deriv2(spl_,v_,acc);
    }else if(order>2){
        std::cerr << "### ERROR : In 'var_interpolated(double, gsl_spline*, int order )', order must be <=0" << std::endl;
    }
    
    if(order==0 && var_i<0){

        std::cerr << "### WARNING : Interpolation value estimated to be negative at point x=" << v_ << std::endl;

        var_i=interpolation_debug(v_,spl_);
    }
    
    gsl_interp_accel_free(acc);
    return var_i;
}
#endif
