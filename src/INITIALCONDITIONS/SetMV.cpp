//  ////////////////////////////////////
// OUTDATED --- DO NOT USE !! //
/////////////////////////////////////////////////

#ifndef __SETMV__CPP__
#define __SETMV__CPP__

////////////////////////////////////////////
//   GAUSSIAN MV MODEL INITIAL CONDITIONS //
////////////////////////////////////////////

#include "../LATTICE/FourierSpace.cpp"

namespace InitialConditions{

    ///////////////////////////////
    // SET TARGET WILSON LINES U //
    ///////////////////////////////
    
    void SetTarget(GaugeLinks *U,GaugeLinks *Utemp){
        
        // RESET TARGET LINKS //
        U->SetIdentity();
        
        // LOOP OVER SLICES IN RAPIDITY //
        for(INT RapSlice=0;RapSlice<Lattice::NRap;RapSlice++){
            
            if(RapSlice%10==0){
                std::cerr << "# BEGINNING COMPUTATION FOR SLICE NRap=" << RapSlice << " OF " << Lattice::NRap << std::endl;
            }
            
            // SET RHO FIELDS /
            for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                
                for(INT y=0;y<U->N[1];y++){
                    for(INT x=0;x<U->N[0];x++){
                        
                        // OPTION FOR INFINITE MV //
                        FourierSpace::RhoT->SetXc(x,y,a,RandomNumberGenerator::Gauss(g2muT/(Lattice::a[0]*sqrt(Lattice::NRap)))*invfmtoGeV); // GeV^2 //
                        // END OPTION //
                    }
                }
                
            }
            
            //std::cerr << "# PERFORMING FOURIER TRANSFORM OF TARGET RHO VARIABLES" << std::endl;
            
            // FOURIER TRANSFORM RHO TO MOMENTUM SPACE //
            FourierSpace::RhoT->ExecuteXtoP();
            
            // RENORMALIZE AFTER X->P -- [d^2x] = [a^2/hbarc^2] = GeV^-2 //
            for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                for(INT y=0;y<U->N[1];y++){
                    for(INT x=0;x<U->N[0];x++){
                        
                        COMPLEX RhoTTEMP=FourierSpace::RhoT->GetP(x,y,a); // GeV^2 //
                        FourierSpace::RhoT->SetP(x,y,a,RhoTTEMP*COMPLEX(Lattice::a[0]*Lattice::a[1]*fmtoinvGeV*fmtoinvGeV)); // DIMLESS //
                    }
                }
                
            }
            // SET RHO OVER DERIVATIVE //
            for(INT pXIndex=0;pXIndex<Lattice::N[0];pXIndex++){
                for(INT pYIndex=0;pYIndex<Lattice::N[1];pYIndex++){
                    
                    // DEFINE MOMENTUM FACTOR // PAGE 1054 NR //
                    DOUBLE DerivativeFactorSqr=(cos(2.0*PI*pXIndex/Lattice::N[0])+cos(2.0*PI*pYIndex/Lattice::N[1])-2.0)/(-0.5*Lattice::a[0]*Lattice::a[1])*invfmtoGeV*invfmtoGeV; // GeV^2 //

                    if(RegMass==0){
                        if(DerivativeFactorSqr!=0.0){
                            
                            for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                                // SAVE MOMENTUM SPACE RHO //
                                COMPLEX rhoTemp=FourierSpace::RhoT->GetP(pXIndex,pYIndex,a);
                                
                                // SAVE RHO/DERIVATIVE //
                                FourierSpace::RhoT->SetP(pXIndex,pYIndex,a,rhoTemp/DerivativeFactorSqr);
                                
                            }
                        
                        }
                        else{
                            
                            // SET ZERO POINT //
                            for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                                FourierSpace::RhoT->SetP(0,0,a,COMPLEX(0.0));
                            }
                        
                        }
                    }
                    else{
                        
                        for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                            // SAVE MOMENTUM SPACE RHO //
                            COMPLEX rhoTemp=FourierSpace::RhoT->GetP(pXIndex,pYIndex,a);
                            
                            // SAVE RHO/DERIVATIVE //
                            FourierSpace::RhoT->SetP(pXIndex,pYIndex,a,rhoTemp/(DerivativeFactorSqr+RegMass*RegMass));
                            
                        }
                    
                    }
                }
            }

            // FOURIER TRANSFORM BACK //
            FourierSpace::RhoT->ExecutePtoX();
            
            // RENORMALIZE AFTER P->X -- [d^2p] = [hbarc^2/(Na)^2] = GeV^2 //
            for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                for(INT y=0;y<U->N[1];y++){
                    for(INT x=0;x<U->N[0];x++){
                        
                        COMPLEX RhoTTEMP=FourierSpace::RhoT->GetX(x,y,a); // GeV^-2
                        FourierSpace::RhoT->SetXc(x,y,a,RhoTTEMP*invfmtoGeV*invfmtoGeV/COMPLEX(Lattice::N[0]*Lattice::N[1]*Lattice::a[0]*Lattice::a[1])); // DIMLESS //
                        
                    }
                }
            }
            
            // RESET TEMP TARGET FIELDS //
            Utemp->SetIdentity();
            
            // EXPONENTIATE SOLUTION AND SAVE TO TEMP TARGET FIELD //
            for(INT y=0;y<U->N[1];y++){
                for(INT x=0;x<U->N[0];x++){
                    
                    // CREATE ALGEBRA alpha ARRAY FOR EXPONENTIATION//
                    SU_Nc_ALGEBRA_FORMAT alphaTemp[SUNcAlgebra::VectorSize];
                    for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                        
                        // SET TARGET FIELDS //
                        alphaTemp[a]=real(FourierSpace::RhoT->GetX(x,y,a));
                        
                    }
                    // EXPONENTIATE TO GAUGE LINK //
                    SUNcAlgebra::Operations::MatrixIExp(1.0,alphaTemp,Utemp->Get(x,y,0));
                    
                    //MATRIX BUFFERS
                    SU_Nc_FUNDAMENTAL_FORMAT Buffer[SUNcGroup::MatrixSize];
                    
                    //COMPUTE UPDATED LINK AND SAVE TO PATH ORDER //
                    SUNcGroup::Operations::UU(U->Get(x,y,0),Utemp->Get(x,y,0),Buffer);
                    
                    // COPY BUFFER TO UPDATE PATH ORDERED LINK V //
                    COPY_SUNcMatrix(U->Get(x,y,0),Buffer);
                }
            }
            
            // OPTION TO MEASURE Qs AT EACH ADDITIONAL SLICE IN Qs //
            //GetQsT(TargetFields::U); // OBSOLETE //
            // END OPTION //
            
        }// END RAPIDITY LOOP

    }
    
    
    /////////////////////////////
    // SET PROJECTILE FIELDS A //
    /////////////////////////////
    
    void SetProjectile(){
        
        // SET PROJECTILE PARAMETERS //
        // ECCENTRICITY //
        DOUBLE aP=1.0; // DIMLESS //
        DOUBLE bP=1.0; // DIMLESS //
        
        // PROTON SIZE PARAMETER -- R_p~\Lambda~1/(g2muP)//
        DOUBLE Rp=DOUBLE(1.0/g2muP); // GeV^-1 //
        
        // SET RHO FIELDS /
        for(INT y=0;y<ProjSolution::A->N[1];y++){
            for(INT x=0;x<ProjSolution::A->N[0];x++){
                
                // SET MIDPOINT FOR LOCALIZED COLOR CHARGE DISTRIBUTION//
                DOUBLE xMid=Lattice::a[0]*Lattice::N[0]/2*fmtoinvGeV; // GeV^-1 //
                DOUBLE yMid=Lattice::a[1]*Lattice::N[1]/2*fmtoinvGeV; // GeV^-1 //
                DOUBLE xPhys=x*Lattice::a[0]*fmtoinvGeV; // GeV^-1 //
                DOUBLE yPhys=y*Lattice::a[1]*fmtoinvGeV; // GeV^-1 //

                for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                    
                    // OPTION FOR LOCALIZED COLOR CHARGE DISTRIBUTION  //
                    DOUBLE arg=std::pow((xPhys-xMid)/(aP*Rp),2.0)+std::pow((yPhys-yMid)/(bP*Rp),2.0); // DIMLESS //
                    FourierSpace::RhoP->SetX(x,y,a,RandomNumberGenerator::Gauss(g2muP/Lattice::a[0]*invfmtoGeV)*exp(-arg/2.0)); // GeV^2 //
                    // END OPTION //

                    // OPTION FOR UNIFORM COLOR CHARGE DISTRIBUTION  //
                    //FourierSpace::RhoP->SetX(x,y,a,RandomNumberGenerator::Gauss(g2muP/Lattice::a[0]));
                    // END OPTION //

                }
                
            }
        }
        
        //std::cerr << "#PERFORMING FOURIER TRANSFORM OF PROJECTILE RHO VARIABLES" << std::endl;
        
        // FOURIER TRANSFORM RHOS TO MOMENTUM SPACE //
        FourierSpace::RhoP->ExecuteXtoP();
        
        // RENORMALIZE AFTER X->P -- [d^2x] = [a^2/hbarc^2] = GeV^-2 //
        for(INT y=0;y<ProjSolution::A->N[1];y++){
            for(INT x=0;x<ProjSolution::A->N[0];x++){
                for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                    
                    COMPLEX RhoPTEMP=FourierSpace::RhoP->GetP(x,y,a); // GeV^2 //
                    FourierSpace::RhoP->SetP(x,y,a,RhoPTEMP*COMPLEX(Lattice::a[0]*Lattice::a[1]*fmtoinvGeV*fmtoinvGeV)); // DIMLESS //
                }
            }
            
        }
        
        // SET RHO OVER DERIVATIVE //
        for(INT pXIndex=0;pXIndex<Lattice::N[0];pXIndex++){
            for(INT pYIndex=0;pYIndex<Lattice::N[1];pYIndex++){
                
                // DEFINE MOMENTUM FACTOR // PAGE 1054 NR //
                DOUBLE DerivativeFactorSqr=(cos(2.0*PI*pXIndex/Lattice::N[0])+cos(2.0*PI*pYIndex/Lattice::N[1])-2.0)/(-0.5*Lattice::a[0]*Lattice::a[1])*invfmtoGeV*invfmtoGeV; // GeV^2 //

                if(RegMass==0){
                    if(DerivativeFactorSqr!=0.0){
                        for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                            
                            // SAVE MOMENTUM SPACE RHO //
                            COMPLEX rhoTemp=FourierSpace::RhoP->GetP(pXIndex,pYIndex,a);
                            
                            // SAVE RHO/DERIVATIVE //
                            FourierSpace::RhoP->SetP(pXIndex,pYIndex,a,rhoTemp/DerivativeFactorSqr);
                            
                        }
                        
                    }
                    else{
                        
                        // SET ZERO POINT //
                        for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                            FourierSpace::RhoP->SetP(0,0,a,COMPLEX(0.0));
                        }
                        
                    }
                }
                else{
                    for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                        
                        // SAVE MOMENTUM SPACE RHO //
                        COMPLEX rhoTemp=FourierSpace::RhoP->GetP(pXIndex,pYIndex,a);
                        
                        // SAVE RHO/DERIVATIVE //
                        FourierSpace::RhoP->SetP(pXIndex,pYIndex,a,rhoTemp/(DerivativeFactorSqr+RegMass*RegMass));
                        
                    }
                    
                }
            }
        }
        
        // FOURIER TRANSFORM BACK //
        FourierSpace::RhoP->ExecutePtoX();
        
        
        // RENORMALIZE AFTER P->X -- [d^2p] = [hbarc^2/(Na)^2] = GeV^2 //
        for(INT y=0;y<ProjSolution::A->N[1];y++){
            for(INT x=0;x<ProjSolution::A->N[0];x++){
                for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                    
                    COMPLEX RhoPTEMP=FourierSpace::RhoP->GetX(x,y,a); // GeV^-2
                    FourierSpace::RhoP->SetXc(x,y,a,RhoPTEMP*invfmtoGeV*invfmtoGeV/COMPLEX(Lattice::N[0]*Lattice::N[1]*Lattice::a[0]*Lattice::a[1])); // DIMLESS //
                    
                }
            }
        }
        
        // SAVE TO SUNc VECTOR //
        for(INT y=0;y<ProjSolution::A->N[1];y++){
            for(INT x=0;x<ProjSolution::A->N[0];x++){
                for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                    ProjSolution::A->Get(x,y,0,a)[0]=real(FourierSpace::RhoP->GetX(x,y,a));
                }
            }
        }
  
    }
    
    void SetMVTarget(){
        
        ///////////////////////////////
        //   SET INITIAL CONDITIONS  //
        ///////////////////////////////
        
        std::cerr << "## SETTING MV TARGET" << std::endl;
        
        SetTarget(TargetFields::U,TempTargetFields::U);
        
        std::cerr << "## FINISHED SETTING MV TARGET" << std::endl;
        
        //GetQsT(TargetFields::U);
        
    }
    
    void SetMVProjectile(){
        
        ///////////////////////////////
        //   SET INITIAL CONDITIONS  //
        ///////////////////////////////
        
        std::cerr << "## SETTING MV PROJECTILE" << std::endl;
        
        SetProjectile();
        
        std::cerr << "## FINISHED SETTING MV PROJECTILE" << std::endl;
        
    }
    
}


#endif
