// Cell.cpp is part of the IP-Glasma solver.
// Copyright (C) 2012 Bjoern Schenke.
// NOTE -- THIS HAS BEEN MODIFIED FROM ITS ORIGINAL VERSION BY BY M. MACE 2019

#include "Cell.h"

Cell::Cell(int N)
{
    locNc = N;
    //int Nc2m1 = locNc*locNc-1;
    g2mu2A = new double;
    g2mu2B = new double;
    TpA = new double;
    TpB = new double;
}

Cell::~Cell()
{
    delete g2mu2A;
    delete g2mu2B;
    delete TpA;
    delete TpB;

}

