// GIPSLattice.cpp is part of the IP-Glasma solver.
// Copyright (C) 2012 Bjoern Schenke.
// NOTE -- THIS HAS BEEN MODIFIED FROM ITS ORIGINAL VERSION BY BY M. MACE 2019

#include "GIPSLattice.h"

//constructor
GIPSLattice::GIPSLattice(Parameters *param, int N, int length){
    
    locNc = N;
    size = length*length;
    double a = param->getL()/static_cast<double>(length);
    
    std::cerr << "# ALLOCATING LATTICE FOR INITIAL FIELDS OF SIZE " << length << "x" << length << " WITH a=" << a << " fm" << std::endl;;
    
    // initialize the array of cells
    //  cells = new Cell*[size];
    for(int i=0; i<size; i++)
    {
        Cell* cell;
        cell = new Cell(locNc);
        cells.push_back(cell);
    }
    
    //std::cerr << "# FINISHED ALLOCATING INITAL LATTICE" << endl;
}

GIPSLattice::~GIPSLattice()
{
    for(int i=0; i<size; i++)
    delete cells[i];
    cells.clear();
    //delete[] cells;
}

