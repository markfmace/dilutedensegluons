// GlauberIPSat.cpp contains portions of the IP-Glasma solver.
// Copyright (C) 2012 Bjoern Schenke.
// NOTE -- THIS HAS BEEN MODIFIED FROM ITS ORIGINAL VERSION BY BY M. MACE 2019

#include <stdio.h>
#include <string>
#include <cmath>
#include <ctime>
#include <iostream>
#include <complex>
#include <fstream>
#include <vector>

#include "Setup.h"
#include "Init.h"
#include "Random.h"
#include "FFT.h"
#include "Parameters.h"
#include "Matrix.h"
#include "GIPSLattice.h"
#include "Spinor.h"

#define _SECURE_SCL 0
#define _HAS_ITERATOR_DEBUGGING 0
using namespace std;

int readInput(string InputFile,Setup *setup, Parameters *param, string OutDirectory,long int RNG_SEED);

// main program
double Initialize(string InputFile,string OutDirectory,long int RNG_SEED, double *PROJ_g2mu2,double *TARG_g2mu2){
    
    Setup *setup;
    setup = new Setup();
    Random *random;
    random = new Random();
    Parameters *param;
    param = new Parameters();
    
    // read parameters from file
    readInput(InputFile,setup,param,OutDirectory,RNG_SEED);
    
    int nn[2];
    int pos;
    nn[0]=param->getSize();
    nn[1]=param->getSize();
    
    stringstream strup_name;
    string up_name;
    up_name = strup_name.str();
    
    //initialize init object
    Init *init;
    init = new Init(nn);
    
    // initialize group
    Group *group;
    group = new Group(param->getNc());
    
    // initialize Glauber class
    Glauber *glauber;
    glauber = new Glauber;
    
    // initialize Glauber
    glauber->initGlauber(param->getSigmaNN(), param->getTarget(), param->getProjectile(), param->getb(), 100,RNG_SEED);
    //std::cerr << "# GLAUBER FINSIHED" << endl;
    
    param->setSuccess(0);

    while(param->getSuccess()==0){
        
        //std::cerr << "#### BEGINNING INITIALIZATION" << endl;

        // RESET SUCCESS INDICATOR //
        param->setSuccess(0);
        // allocate lattice
        GIPSLattice *lat;
        lat = new GIPSLattice(param, param->getNc(), param->getSize());
        
        //initialize random generator using time and seed from input file
        unsigned long long int rnum;
        
        if(param->getUseTimeForSeed()==1){
            rnum=time(0)+RNG_SEED;
        }
        else{
            // NO GUARANTEE THIS RUNS //
            rnum=RNG_SEED;
        }
        random->init_genrand64(rnum);
        
        
        ofstream fout1(up_name.c_str(),ios::app);
        fout1 << "Random seed used: " << param->getRandomSeed() << endl;
        fout1.close();
        
        // initialize gsl random number generator (used for non-Gaussian distributions)
        random->gslRandomInit(rnum);
        
        // initialize U-fields on the lattice
        //      init->init(lat, group, param, random, glauber);
        int READFROMFILE = 0;
        init->init(lat, group, param, random, glauber, READFROMFILE,OutDirectory,RNG_SEED);
        //std::cerr << "# INITIALIZATION FINISHED" << endl;
        
        if(param->getSuccess()==0){
            
            std::cerr << "# RESAMPLING COLOR CONFIGURATIONS" << std::endl;
            delete lat;
            continue;
        }
        
        delete init;
        delete random;
        delete glauber;
        
        // COPY VALUES FROM IP-GLASMA BASED GLAUBER+IP-SAT TO DD SOLVER //
        std::cerr << "# SETTING FINAL g2mu2 FIELDS " << std::endl;
        for(pos=0;pos<param->getSize()*param->getSize();pos++){

            PROJ_g2mu2[pos]=lat->cells[pos]->getg2mu2A(); // DIMLESS //
            TARG_g2mu2[pos]=lat->cells[pos]->getg2mu2B(); // DIMLESS //
        }
        
        delete lat;
    }
    
    //MPI_Barrier(MPI_COMM_WORLD);
    
    delete group;
    //delete evolution;
    delete param;
    delete setup;
    //delete myeigen;
    //cout << "done." << endl;
    //return 1;
    // RETURN IMPACT PARAMETER //
    return param->getb();
    
}/* main */


int readInput(string file_name,Setup *setup, Parameters *param, string OutDirectory,long int RNG_SEED){

    param->setNucleusQsTableFileName(setup->StringFind(file_name,"NucleusQsTableFileName"));
    param->setNucleonPositionsFromFile(setup->IFind(file_name,"nucleonPositionsFromFile"));
    param->setTarget(setup->StringFind(file_name,"Target"));
    param->setProjectile(setup->StringFind(file_name,"Projectile"));
    param->setMode(setup->IFind(file_name,"mode"));
    param->setRunningCoupling(setup->IFind(file_name,"runningCoupling"));
    param->setL(setup->DFind(file_name,"L"));
    param->setBG(setup->DFind(file_name,"BG"));
    param->setBGq(setup->DFind(file_name,"BGq"));
    param->setMuZero(setup->DFind(file_name,"muZero"));
    param->setc(setup->DFind(file_name,"c"));
    param->setSize(setup->IFind(file_name,"size"));
    param->setUseFluctuatingx(setup->IFind(file_name,"useFluctuatingx"));
    param->setNc(setup->IFind(file_name,"Nc"));
    param->setSeed(setup->ULLIFind(file_name,"seed"));
    param->setUseSeedList(setup->IFind(file_name,"useSeedList"));
    param->setRoots(setup->DFind(file_name,"roots"));
    param->setg(setup->DFind(file_name,"g"));
    param->setJacobianm(setup->DFind(file_name,"Jacobianm"));
    param->setSigmaNN(setup->DFind(file_name,"SigmaNN"));
    param->setRmax(setup->DFind(file_name,"rmax"));
    param->setbmin(setup->DFind(file_name,"bmin"));
    param->setbmax(setup->DFind(file_name,"bmax"));
    param->setQsmuRatio(setup->DFind(file_name,"QsmuRatio"));
    param->setUsePseudoRapidity(setup->DFind(file_name,"usePseudoRapidity"));
    param->setRapidity(setup->DFind(file_name,"Rapidity"));
    param->setUseNucleus(setup->IFind(file_name,"useNucleus"));
    param->setg2mu(setup->DFind(file_name,"g2mu"));
    param->setRunWithQs(setup->IFind(file_name,"runWith0Min1Avg2MaxQs"));
    param->setRunWithkt(setup->IFind(file_name,"runWithkt"));
    param->setRunWithLocalQs(setup->IFind(file_name,"runWithLocalQs"));
    param->setRunWithThisFactorTimesQs(setup->DFind(file_name,"runWithThisFactorTimesQs"));
    param->setxFromThisFactorTimesQs(setup->DFind(file_name,"xFromThisFactorTimesQs"));
    param->setLinearb(setup->IFind(file_name,"samplebFromLinearDistribution"));
    param->setAverageOverNuclei(setup->IFind(file_name,"averageOverThisManyNuclei"));
    param->setUseTimeForSeed(setup->IFind(file_name,"useTimeForSeed"));
    param->setUseFixedNpart(setup->IFind(file_name,"useFixedNpart"));
    param->setSmearQs(setup->IFind(file_name,"smearQs"));
    param->setSmearingWidth(setup->DFind(file_name,"smearingWidth"));
    param->setGaussianWounding(setup->IFind(file_name,"gaussianWounding"));
    param->setProtonAnisotropy(setup->DFind(file_name,"protonAnisotropy"));
    param->setUseConstituentQuarkProton(setup->DFind(file_name,"useConstituentQuarkProton"));
    std::cerr << "# PARAMETERS READ IN FROM FILE " << file_name << endl;
    
    // write the used parameters into file "usedParameters.dat" as a double check for later
    time_t rawtime = time(0);
    stringstream strup_name;
    strup_name << OutDirectory << "/usedParameters" << RNG_SEED << ".dat";
    string up_name;
    up_name = strup_name.str();
    
    fstream fout1(up_name.c_str(),ios::out);
    char * timestring = ctime(&rawtime);
    fout1 << "File created on " << timestring << endl;
    fout1 << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ " << endl;
    fout1 << "Used parameters by MCG+IP-Sat" << endl;
    fout1 << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ " << endl;
    fout1 << " " << endl;
    fout1 << " Output by readInput in main.cpp: " << endl;
    fout1 << " " << endl;
    fout1 << "Program run in mode " << param->getMode() << endl;
    fout1 << "Nc " << param->getNc() << endl;
    fout1 << "size " << param->getSize() << endl;
    fout1 << "lattice spacing a " << param->getL()/static_cast<double>(param->getSize()) << " fm " << endl;
    fout1 << "Projectile " << param->getProjectile() << endl;
    fout1 << "Target " << param->getTarget() << endl;
    fout1 << "Gaussian wounding " << param->getGaussianWounding() << endl;
    fout1 << "Using fluctuating x=Qs/root(s) " << param->getUseFluctuatingx() << endl;
    if(param->getRunWithkt()==0){
        fout1 << "Using local Qs to run " << param->getRunWithLocalQs() << endl;
    }
    else{
        fout1 << "running alpha_s with k_T" << endl;
    }
    fout1 << "QsmuRatio " << param->getQsmuRatio() << endl;
    fout1 << "smeared mu " << param->getSmearQs() << endl;
    fout1 << "rmax " << param->getRmax() << endl;
    if(param->getSmearQs()==1){
        fout1 << "smearing width " << param->getSmearingWidth() << endl;
    }
    fout1.close();
    
    return 0;
}
