// Cell.h is part of the IP-Glasma solver.
// Copyright (C) 2012 Bjoern Schenke.
// NOTE -- THIS HAS BEEN MODIFIED FROM ITS ORIGINAL VERSION BY BY M. MACE 2019

#ifndef Cell_h
#define Cell_h

#include <complex>
#include <iostream>
#include <cstdlib>
#include <vector>

#include "Matrix.h"

using namespace std;

class Cell
{
    private:
    
    int locNc;
    
    // nucleus A
    double *g2mu2A;  // color charge density of nucleus A
    double *TpA;     // sum over the proton T(b) in this cell for nucleus A
    
    // nucleus B
    double *g2mu2B;  // color charge density of nucleus B
    double *TpB;     // sum over the proton T(b) in this cell for nucleus B

    //  bool parity; // Parity of the cell (needed for Gauge fixing)
   
    public:
    Cell(int N);
    ~Cell();
    
    void setg2mu2A(double in) { *g2mu2A = in; };
    void setg2mu2B(double in) { *g2mu2B = in; };
    
    double getg2mu2A() { return *g2mu2A; };
    double getg2mu2B() { return *g2mu2B; };
    
    void setTpA(double in) { *TpA = in; };
    void setTpB(double in) { *TpB = in; };
    
    double getTpA() { return *TpA; };
    double getTpB() { return *TpB; };
    
};

#endif

