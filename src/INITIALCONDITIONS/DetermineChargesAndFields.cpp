// DetermineChargesAndFields.cpp is part of Dilute-Dense Solver
// Copyright (C) 2019 M. Mace

#ifndef __DETERMINECHARGESANDFIELDS__CPP__
#define __DETERMINECHARGESANDFIELDS__CPP__

////////////////////////////////////////////////////////////
//    DETERMINE COLOR CHARGES FROM COLOR CHARGE DENSITIES //
//                 AND SOLVE FOR FIELDS                   //
////////////////////////////////////////////////////////////

#include "../LATTICE/FourierSpace.cpp"

double Initialize(std::string InputFile,std::string OutDirectory,long int RNG_SEED, double *PROJ_g2mu2,double *TARG_g2mu2);


namespace InitialConditions{

    ///////////////////////////////
    // SET TARGET WILSON LINES U //
    ///////////////////////////////

    void SetTarget(WilsonLines *U,WilsonLines *Utemp,VectorFields *gSQRmu){
        
        // RESET TARGET LINKS //
        U->SetIdentity();
        
        // LOOP OVER SLICES IN RAPIDITY //
        for(INT RapSlice=0;RapSlice<Lattice::NRap;RapSlice++){
            if(RapSlice%20==0){
                std::cerr << "# BEGINNING COMPUTATION FOR SLICE NRap=" << RapSlice << " OF " << Lattice::NRap << std::endl;
            }
            
            // SET RHO FIELDS /
            for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                for(INT y=0;y<U->N[1];y++){
                    for(INT x=0;x<U->N[0];x++){
                        
                        // OPTION FOR UNIFORM COLOR CHARGE DISTRIBUTION -- INFINTIE MV-TARGET  //
                        #if IC_FLAG==MV_FLAG
                        FourierSpace::RhoT->SetXc(x,y,a,RandomNumberGenerator::Gauss(g2muT/(Lattice::a[0]*sqrt(Lattice::NRap)))*invfmtoGeV); // GeV^2 //
                        #endif
                        // END OPTION //

                        // OPTION FOR GLAUBER--IP-SAT LOCALIZED COLOR CHARGE DISTRIBUTION  //
                        // [g^2 \mu]= GeV -- GeV/fm -> GeV^2 //
                        #if IC_FLAG==GIPS_FLAG
                        DOUBLE Rho0=gSQRmu->Get(x,y,0,0)[0]/(Lattice::a[0]*sqrt(Lattice::NRap))*invfmtoGeV; // GeV^2 //
                        FourierSpace::RhoT->SetXc(x,y,a,Rho0*RandomNumberGenerator::Gauss(1.0)); // GeV^2 //
                        #endif
                        // END OPTION //
                        
                        // OPTION FOR DEBUGGING INFINITE MV //
                        //FourierSpace::RhoT->SetXc(x,y,a,(3.24*x*x+y*y*10.2+48.*x*y+12.123*a*a+420.*a*x)/sqrt(Lattice::NRap));
                        // END OPTION //

                    }
                }
                
            }
            
            // FOURIER TRANSFORM RHO TO MOMENTUM SPACE -- NO FFTW NORMALIZATION NEEDED //
            FourierSpace::RhoT->ExecuteXtoP();
            
            // RENORMALIZE AFTER X->P -- [d^2x] = [a^2/hbarc^2] = GeV^-2 //
            for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                for(INT y=0;y<U->N[1];y++){
                    for(INT x=0;x<U->N[0];x++){
                        
                        COMPLEX RhoTTEMP=FourierSpace::RhoT->GetP(x,y,a); // GeV^2 //
                        FourierSpace::RhoT->SetP(x,y,a,RhoTTEMP*COMPLEX(Lattice::a[0]*Lattice::a[1]*fmtoinvGeV*fmtoinvGeV)); // DIMLESS //
                    }
                }
                
            }
            
            // SET RHO OVER DERIVATIVE //
            //#pragma omp parallel for collapse(2)
            for(INT pXIndex=0;pXIndex<Lattice::N[0];pXIndex++){
                for(INT pYIndex=0;pYIndex<Lattice::N[1];pYIndex++){
                    
                    // DEFINE MOMENTUM FACTOR // PAGE 1054 NR //
                    DOUBLE DerivativeFactorSqr=(cos(2.0*PI*pXIndex/Lattice::N[0])+cos(2.0*PI*pYIndex/Lattice::N[1])-2.0)/(-0.5*Lattice::a[0]*Lattice::a[1])*invfmtoGeV*invfmtoGeV; // GeV^2 //
                    
                    if(RegMass==0){
                        if(DerivativeFactorSqr!=0.0){
                            
                            for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                                
                                // SAVE MOMENTUM SPACE RHO //
                                COMPLEX rhoTemp=FourierSpace::RhoT->GetP(pXIndex,pYIndex,a); // DIMLESS //
                                
                                // SAVE RHO/DERIVATIVE^2 //
                                FourierSpace::RhoT->SetP(pXIndex,pYIndex,a,rhoTemp/DerivativeFactorSqr); // GeV^-2 //
                                
                            }
                            
                        }
                        else{
                            
                            // SET ZERO POINT //
                            for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                                FourierSpace::RhoT->SetP(0,0,a,COMPLEX(0.0));
                            }
                            
                        }
                    }
                    else{
                        
                        for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                            
                            // SAVE MOMENTUM SPACE RHO //
                            COMPLEX rhoTemp=FourierSpace::RhoT->GetP(pXIndex,pYIndex,a); // DIMLESS //
                            
                            // SAVE RHO/DERIVATIVE //
                            DOUBLE mRegSqr=RegMass*RegMass; // GeV^2 //
                            FourierSpace::RhoT->SetP(pXIndex,pYIndex,a,rhoTemp/(DerivativeFactorSqr+mRegSqr));  // GeV^-2 //
                            
                        }
                        
                    }
                }
            }
            
            // FOURIER TRANSFORM BACK //
            FourierSpace::RhoT->ExecutePtoX();

            // RENORMALIZE AFTER P->X -- [d^2p] = [hbarc^2/(Na)^2] = GeV^2 //
            for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                for(INT y=0;y<U->N[1];y++){
                    for(INT x=0;x<U->N[0];x++){
                        
                        COMPLEX RhoTTEMP=FourierSpace::RhoT->GetX(x,y,a); // GeV^-2
                        FourierSpace::RhoT->SetXc(x,y,a,RhoTTEMP*invfmtoGeV*invfmtoGeV/COMPLEX(Lattice::N[0]*Lattice::N[1]*Lattice::a[0]*Lattice::a[1])); // DIMLESS //
                        
                    }
                }
            }
            
            
            // RESET TEMP TARGET FIELDS //
            Utemp->SetIdentity();
            
            // EXPONENTIATE SOLUTION AND SAVE TO TEMP TARGET FIELD //
            //#pragma omp parallel for collapse(2)
            for(INT y=0;y<U->N[1];y++){
                for(INT x=0;x<U->N[0];x++){
                    // CREATE ALGEBRA alpha ARRAY FOR EXPONENTIATION//
                    SU_Nc_ALGEBRA_FORMAT alphaTemp[SUNcAlgebra::VectorSize];
                    
                    for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                        
                        // SET TARGET FIELDS //
                        // NORMALIZATION NEEDED FOR FFTW //
                        alphaTemp[a]=real(FourierSpace::RhoT->GetX(x,y,a)); // DIMLESS //
                        
                    }
                    // EXPONENTIATE TO GAUGE LINK //
                    SUNcAlgebra::Operations::MatrixIExp(1.0,alphaTemp,Utemp->Get(x,y,0));
                    
                    //MATRIX BUFFERS
                    SU_Nc_FUNDAMENTAL_FORMAT Buffer[SUNcGroup::MatrixSize];
                    
                    //COMPUTE UPDATED LINK AND SAVE TO PATH ORDER //
                    SUNcGroup::Operations::UU(U->Get(x,y,0),Utemp->Get(x,y,0),Buffer);
                    
                    // COPY BUFFER TO UPDATE PATH ORDERED LINK V //
                    COPY_SUNcMatrix(U->Get(x,y,0),Buffer);
                }
            }
            
        }
        
    }

    /////////////////////////////
    // SET PROJECTILE FIELDS A //
    /////////////////////////////

    void SetProjectile(VectorFields *gSQRmu){
        
        // PARAMETERS FOR MV-PROTON OPTION //
        // SET PROJECTILE PARAMETERS //
        // ECCENTRICITY //
        #if IC_FLAG==MV_FLAG
        DOUBLE aP=1.0; // DIMLESS //
        DOUBLE bP=1.0; // DIMLESS //
        
        // PROTON SIZE PARAMETER //
//        DOUBLE Rp=DOUBLE(1.0/g2muP); // R_p~\Lambda~1/(g2muP) GeV^-1 //
        DOUBLE Rp=2.0; // BG=Rp^2=4 GeV^-2 -> Rp= 2 GeV^-1 //
        std::cerr << "# Rp=" << Rp << std::endl;
        #endif
        // END OPTION //

        // SET RHO FIELDS /
        for(INT y=0;y<ProjSolution::A->N[1];y++){
            for(INT x=0;x<ProjSolution::A->N[0];x++){
                
                // OPTION FOR MV-PROTON //
                // SET MIDPOINT FOR LOCALIZED COLOR CHARGE DISTRIBUTION/ /
                #if IC_FLAG==MV_FLAG
                DOUBLE xMid=Lattice::a[0]*Lattice::N[0]/2.0*fmtoinvGeV; // GeV^-1 //
                DOUBLE yMid=Lattice::a[1]*Lattice::N[1]/2.0*fmtoinvGeV; // GeV^-1 //
                DOUBLE xPhys=x*Lattice::a[0]*fmtoinvGeV; // GeV^-1 //
                DOUBLE yPhys=y*Lattice::a[1]*fmtoinvGeV; // GeV^-1 //
                #endif
                // END OPTION //

                for(INT a=0;a<SUNcAlgebra::VectorSize;a++){

                    // OPTION FOR MV-PROTON //
                    #if IC_FLAG==MV_FLAG
                    DOUBLE arg=std::pow((xPhys-xMid)/(aP*Rp),2.0)+std::pow((yPhys-yMid)/(bP*Rp),2.0); // DIMLESS //
                    FourierSpace::RhoP->SetX(x,y,a,RandomNumberGenerator::Gauss(g2muP/Lattice::a[0]*invfmtoGeV)*exp(-arg/2.0)); // GeV^2 //
                    #endif
                    // END OPTION //
                    
                    // OPTION FOR GLAUBER--IP-SAT LOCALIZED COLOR CHARGE DISTRIBUTION  //
                    // [g^2 \mu]= GeV -- GeV/fm -> GeV^2 //
                    #if IC_FLAG==GIPS_FLAG
                    DOUBLE Rho0=gSQRmu->Get(x,y,0,0)[0]/Lattice::a[0]*invfmtoGeV; // GeV^2 //
                    FourierSpace::RhoP->SetX(x,y,a,Rho0*RandomNumberGenerator::Gauss(1.0)); // GeV^2 //
                    #endif
                    // END OPTION //
                    
                    // OPTION FOR UNIFORM COLOR CHARGE DISTRIBUTION -- INFINTIE MV-PROJECTILE  //
                    // #if IC_FLAG==MV_FLAG
                    //FourierSpace::RhoP->SetX(x,y,a,RandomNumberGenerator::Gauss(g2muP/Lattice::a[0]*invfmtoGeV));
                    // #endif
                    // END OPTION //
                    
                    // OPTION FOR DEBUGGING MV //
                    //FourierSpace::RhoP->SetX(x,y,a,(5.3*x*x+y*y*9.4+3.431*a*a+69.*y*a));
                    // END OPTION //

                }
                
            }
        }
        
        //std::cerr << "# PERFORMING FOURIER TRANSFORM OF PROJECTILE RHO VARIABLES" << std::endl;
        
        // FOURIER TRANSFORM RHOS TO MOMENTUM SPACE //
        FourierSpace::RhoP->ExecuteXtoP();
        
        // RENORMALIZE AFTER X->P -- [d^2x] = [a^2/hbarc^2] = GeV^-2 //
        for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
            for(INT y=0;y<ProjSolution::A->N[1];y++){
                for(INT x=0;x<ProjSolution::A->N[0];x++){
                    
                    COMPLEX RhoPTEMP=FourierSpace::RhoP->GetP(x,y,a); // GeV^2 //
                    FourierSpace::RhoP->SetP(x,y,a,RhoPTEMP*COMPLEX(Lattice::a[0]*Lattice::a[1]*fmtoinvGeV*fmtoinvGeV)); // DIMLESS //
                }
            }
            
        }
        
        // SET RHO OVER DERIVATIVE //
        //#pragma omp parallel for collapse(2)
        for(INT pXIndex=0;pXIndex<Lattice::N[0];pXIndex++){
            for(INT pYIndex=0;pYIndex<Lattice::N[1];pYIndex++){
                
                // DEFINE MOMENTUM FACTOR // PAGE 1054 NR //
                DOUBLE DerivativeFactorSqr=(cos(2.0*PI*pXIndex/Lattice::N[0])+cos(2.0*PI*pYIndex/Lattice::N[1])-2.0)/(-0.5*Lattice::a[0]*Lattice::a[1])*invfmtoGeV*invfmtoGeV; // GeV^2 //
                
                if(RegMass==0){
                    if(DerivativeFactorSqr!=0.0){
                        
                        for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                            // SAVE MOMENTUM SPACE RHO //
                            COMPLEX rhoTemp=FourierSpace::RhoP->GetP(pXIndex,pYIndex,a); // DIMELSS //
                            
                            // SAVE RHO/DERIVATIVE^2 //
                            FourierSpace::RhoP->SetP(pXIndex,pYIndex,a,rhoTemp/DerivativeFactorSqr); // GeV^-2 //
                            
                        }
                        
                    }
                    else{
                        
                        // SET ZERO POINT //
                        for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                            FourierSpace::RhoP->SetP(0,0,a,COMPLEX(0.0));
                        }
                        
                    }
                }
                else{
                    
                    for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                        // SAVE MOMENTUM SPACE RHO //
                        COMPLEX rhoTemp=FourierSpace::RhoP->GetP(pXIndex,pYIndex,a); // DIMLESS //
                        
                        // SAVE RHO/DERIVATIVE //
                        DOUBLE mRegSqr=RegMass*RegMass; // GeV^2 //
                        FourierSpace::RhoP->SetP(pXIndex,pYIndex,a,rhoTemp/(DerivativeFactorSqr+mRegSqr)); // GeV^-2 //

                    }
                    
                }
            }
        }
        
        // FOURIER TRANSFORM BACK //
        FourierSpace::RhoP->ExecutePtoX();
        
        // RENORMALIZE AFTER P->X -- [d^2p] = [hbarc^2/(Na)^2] = GeV^2 //
        for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
            for(INT y=0;y<ProjSolution::A->N[1];y++){
                for(INT x=0;x<ProjSolution::A->N[0];x++){
                    
                    COMPLEX RhoPTEMP=FourierSpace::RhoP->GetX(x,y,a); // GeV^-2
                    FourierSpace::RhoP->SetXc(x,y,a,RhoPTEMP*invfmtoGeV*invfmtoGeV/COMPLEX(Lattice::N[0]*Lattice::N[1]*Lattice::a[0]*Lattice::a[1])); // DIMLESS //
                    
                }
            }
        }
        
        // SAVE TO SUNc VECTOR //
        //#pragma omp parallel for collapse(3)
        for(INT y=0;y<ProjSolution::A->N[1];y++){
            for(INT x=0;x<ProjSolution::A->N[0];x++){
                for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                    
                    // NORMALIZATION NEEDED FOR FFTW //
                    ProjSolution::A->Get(x,y,0,a)[0]=real(FourierSpace::RhoP->GetX(x,y,a)); // DIMLESS //
                
                }
            }
        }
        
    }

    // GENERAL ROUTINE FOR SETTING CHARGES AND FIELDS //
    void DetermineChargesAndFields(long int RNG_SEED){
        
        ///////////////////////////////
        //   SET INITIAL CONDITIONS  //
        ///////////////////////////////
        
        std::cerr << "## SETTING INITIAL CHARGES AND FIELDS " << std::endl;
        #if IC_FLAG==MV_FLAG
        std::cerr << "## USING MV MODEL" << std::endl;
        #endif
        #if IC_FLAG==GIPS_FLAG
        std::cerr << "## USING GLAUBER+IP-SAT" << std::endl;
        #endif
        
        #if IC_FLAG==GIPS_FLAG
        // SAMPLE NUCLEON POSITIONS AND DETMERINE RHOS //
        DOUBLE *Projectile_g2mu2=new DOUBLE[Lattice::N[0]*Lattice::N[1]];
        DOUBLE *Target_g2mu2=new DOUBLE[Lattice::N[0]*Lattice::N[1]];
        
        bImpact=Initialize(IO::InputFile,IO::OutputDirectory,RNG_SEED,Projectile_g2mu2,Target_g2mu2);
                
        // COPY TO LOCAL FIELDS FROM GLAUBER IPSAT //
        for(INT yLoc=0;yLoc<Lattice::N[1];yLoc++){
            for(INT xLoc=0;xLoc<Lattice::N[0];xLoc++){
                
                INT Index=xLoc*Lattice::N[0]+yLoc;
                
                // GET VALUES FROM GlauberIPSat.cpp (FROM Init.cpp) -- THESE COME IN LATTICE (DIMENSIONLESS) UNITS //
                // g_s==1 -- [Projectile_g2mu2] = DIMLESS -- sqrt(Projectile_g2mu2/a^2)= fm^-1 -> GeV //
                g2mu::Proj->Get(xLoc,yLoc,0,0)[0]=sqrt(Projectile_g2mu2[Index]/(Lattice::a[0]*Lattice::a[1]))*invfmtoGeV;
                g2mu::Targ->Get(xLoc,yLoc,0,0)[0]=sqrt(Target_g2mu2[Index]/(Lattice::a[0]*Lattice::a[1]))*invfmtoGeV;

            }
        }
        
        // CLEAN-UP //
        delete[] Projectile_g2mu2;
        delete[] Target_g2mu2;
        
        //std::cerr << "# FINSIHED SAMPLING NUCLEI " << std::endl;
        
        // OPTION FOR FILE OUTPUT OF RHOS //
        if(OUTPUT_FLAG==1){
            std::cerr << "# OUTPUTTING g2mu " << std::endl;
            IO::SaveConfigurationE("Projg2mu",g2mu::Proj);
            IO::SaveConfigurationE("Targetg2mu",g2mu::Targ);
        }
        #endif
        // END OPTION //
        
        // SET FIELDS FROM g2mu RHO FIELDS //
        //std::cerr << "# SETTING TARGET FIELDS " << std::endl;
        
        SetTarget(TargetFields::U,TempTargetFields::U,g2mu::Targ);
        
        //std::cerr << "# FINSIHED SETTING TARGET FIELDS " << std::endl;
        
        //std::cerr << "# SETTING TARGET FIELDS " << std::endl;
        
        SetProjectile(g2mu::Proj);
        
        //std::cerr << "# FINISHED SETTING TARGET FIELDS " << std::endl;
        
        std::cerr << "## FINISHED SETTING INITIAL CHARGES AND FIELDS" << std::endl;
        
        
    }


}

#endif
