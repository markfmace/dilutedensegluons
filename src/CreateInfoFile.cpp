// CreateInfoFile.cpp is part of Dilute-Dense Solver
// Copyright (C) 2019 M. Mace

#ifndef __CREATE_INFO_FILE_CPP__
#define __CREATE_INFO_FILE_CPP__

void CreateInfoFile(INT SEED){
    
    // CREATE OUTPUT STREAM FOR MULTIPLICITY //
    std::ofstream InfoStream; InfoStream.open(StringManipulation::StringCast(IO::OutputDirectory,"InfoID",SEED,".txt").c_str());
    
    InfoStream << "############ BEGIN INFO FILE ############" << std::endl;
    // LATTICE PARAMETERS //
    InfoStream << "#N=" << Lattice::N[0] << " " << Lattice::N[1] << std::endl;
    InfoStream << "#NRap=" << Lattice::NRap << std::endl;
    InfoStream << "#a=" << Lattice::a[0] << " " << Lattice::a[1] << std::endl;
    InfoStream << "#aeta=" << Lattice::aeta << std::endl;
    
    InfoStream << "#RegMass=" << RegMass << std::endl;

    InfoStream << "#kMin=" << kMin << std::endl;
    InfoStream << "#kMax=" << kMax << std::endl;
    InfoStream << "#kREFMin=" << kREFMin << std::endl;
    InfoStream << "#kREFMax=" << kREFMax << std::endl;
    InfoStream << "#dkBin=" << dkBin << std::endl;
    InfoStream << "#dkStep=" << dkStep << std::endl;
    InfoStream << "#kINT_FLAG=" << kINT_FLAG << std::endl;
    
    #if IC_FLAG==GIPS_FLAG
    InfoStream << "#InputFile=" << IO::InputFile << std::endl;
    #endif
    #if IC_FLAG==MV_FLAG
    InfoStream << "#g2muP=" << g2muP << std::endl;
    InfoStream << "#g2muT=" << g2muT << std::endl;
    #endif
    InfoStream << "############ END INFO FILE ############" << std::endl;
    
    // CLOSE OUTPUT //
    InfoStream.close();
}

#endif
