FOLDER="V22TEST"
for FILE in "pDipole" "Dipole" "SIHist"
do

cd $FOLDER
#REMOVE TARGET
rm Average${FILE}.txt

awk '{a[FNR]+=$1;b[FNR]+=$2;b2[FNR]+=$2*$2;n[FNR]++;}END{for(i=1;i<=FNR;i++) printf "%g %.16g %.16g\n", a[i]/n[i],b[i]/n[i],sqrt(n[i]*b2[i]-b[i]*b[i])/(n[i]*sqrt(n[i]-1));}' ${FILE}ID* | tail -n +4 > Average${FILE}.txt

cd ..

done
