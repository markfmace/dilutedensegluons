import math
import numpy as np
import matplotlib.pyplot as plt

params = {'text.usetex': False, 'mathtext.fontset': 'cm'}
plt.rcParams.update(params)

def plotv2(f,column,mkr,ls,mylabel,scale):
    x = np.genfromtxt(f, usecols=(0))
    S = np.genfromtxt(f, usecols=(column))
    plt.errorbar(x*scale,S,yerr=0,marker=mkr,fmt='o',linestyle=ls,label=mylabel)

def plotv22fromcn(f,mkr,ls,mylabel,scale):
    x = np.genfromtxt(f, usecols=(0))
    c1 = np.genfromtxt(f, usecols=(1))
    c2 = np.genfromtxt(f, usecols=(2))
    c3 = np.genfromtxt(f, usecols=(3))
    c4 = np.genfromtxt(f, usecols=(4))
    v22=np.sqrt(c2)
    
    plt.errorbar(x*scale,v22,yerr=0,marker=mkr,fmt='o',linestyle=ls,label=mylabel)

def plotv24fromcn(f,mkr,ls,mylabel,scale):
    x = np.genfromtxt(f, usecols=(0))
    c1 = np.genfromtxt(f, usecols=(1))
    c2 = np.genfromtxt(f, usecols=(2))
    c3 = np.genfromtxt(f, usecols=(3))
    c4 = np.genfromtxt(f, usecols=(4))
    v24=np.sqrt(np.sqrt(-c4))

    plt.errorbar(x*scale,v24,yerr=0,marker=mkr,fmt='o',linestyle=ls,label=mylabel)

font = {'family': 'serif',
    'weight': 'normal',
        'size': 15,
        }

plt.rc('font', **font)
plt.rcParams.update({'figure.autolayout': True})

plt.figure(1)

#plt.xlim([3,7])
plt.ylim([0,0.2])
#ax.set_xscale("log", nonposx='clip')
#plt.yscale('log')

#plotv2('FROM_NERSC_IPGLASMA/v2.txt',2,'v','--','$v_2\{2\}$',1.0)
#plotv22fromcn('FROM_NERSC_IPGLASMA/c24.txt','o','-','$v_2\{2\}$',1.0)
#plotv24fromcn('FROM_NERSC_IPGLASMA/c24.txt','s','--','$v_2\{4\}$',1.0)
plotv22fromcn('FROM_NERSC_IPGLASMA_2/c24.txt','o','-','$v_2\{2\}$',1.0)
plotv24fromcn('FROM_NERSC_IPGLASMA_2/c24.txt','s','--','$v_2\{4\}$',1.0)


#hist('FROM_NERSC/v2ktrig3.txt',1,'s','-','$k_1=k,k_2=3$',1.0)

plt.legend(frameon=False,loc='upper right',numpoints=1)
plt.xlabel('$k$')
#plt.ylabel('$v_2\{2\}$')

#plt.savefig('v2comp.pdf')

plt.show()

