import numpy as np
import math
from scipy.stats import norm
import matplotlib.mlab as mlab
from matplotlib import pyplot as plt
import glob
import os

# DEFINE V22 FROM FILE
def plotv22fromcn(f,mkr,ls,mylabel,scale):
    x = np.genfromtxt(f, usecols=(0))
    c1 = np.genfromtxt(f, usecols=(1))
    c2 = np.genfromtxt(f, usecols=(2))

    v22=np.sqrt(c2)
    
    plt.errorbar(x*scale,v22,yerr=0,marker=mkr,fmt='o',linestyle=ls,label=mylabel)

# DIRECTORY STRUCTURE
MASTERDIR="BS"
SUBDIR="TESTpAuRUN1"

DIR=MASTERDIR #MASTERDIR+"/"+SUBDIR
print(DIR)

filenames = sorted(glob.glob(DIR+"/Multiplicity*"))
MultArray=np.zeros((len(filenames),2))


# IMPORT MULTPLICITY DATA WITH SEED NUMBER
n=0
for f in filenames:
    base=os.path.basename(f)
    tempf=os.path.splitext(base)[-2]
    SEED=tempf.replace('MultiplicityID','')
    mult=np.genfromtxt(f)
    print(mult[0])
    MultArray[n,0]=float(mult[0])
    MultArray[n,1]=int(SEED)
    n=n+1

#SORT
MultArray=MultArray[np.argsort(MultArray[:, 0])]

# GET TOTAL INTEGRATED MULTIPLICITY
multData=MultArray[:,0]
TotMult=np.sum(multData)

SEEDStoSave=[]
tot=0
row=len(multData)
row=row-1

Percentage=0.05 #0.05
# DETERMINE WHICH RANDOM SEEDS TO SAVE AS A PERCENTAGE OF TOTAL INTEGRATED MULTIPLICTY
# IN FUTURE ADD FEATURE TO SAVE SEED OF PREVIOUS BIN TO DO DIFFERNET CENTRALITY SELECTION
while tot<Percentage*TotMult:
    SEEDStoSave=np.append(SEEDStoSave,MultArray[row,1])
    tot=tot+MultArray[row,0]
    row=row-1

# DETERMINE FILES WHCIH CORRESPOND TO THOSE RNG SEEDS
selectedfiles=[]
for i in range(0,len(SEEDStoSave)):
    fn="SingleInclusiveID"+str(int(SEEDStoSave[i]))+".txt"
    selectedfiles.append(str(fn))

#print(selectedfiles)

# DETERMINE SIZE OF TOTAL DATA WITH SELECTED SEEDS
temp=np.genfromtxt(DIR+"/"+str(selectedfiles[0]))
rows,columns=temp.shape
AllData=np.zeros((len(selectedfiles),rows,columns))

# GET DATA FROM SELECTED SEEDS
n=0
for files in selectedfiles:
    temp=np.genfromtxt(DIR+"/"+str(files))
    for i in range(0,rows):
        for j in range(0,columns):
            AllData[n][i][j]=temp[i][j]
    n=n+1

# AVERAGE DATA FOR SELECTED SEEDS
# ONLY V22 CURRENTLY
AverageData=np.zeros((len(AllData[0,:,0]),2))

for row in range(0,len(AllData[0,:,0])):
    sum0=0
    sum2=0
    n=0
    for dataset in range(0,len(AllData[:,0,0])):
        n=n+1
        sum0=sum0+AllData[dataset][row][0]
        sum2=sum2+AllData[dataset][row][4]*AllData[dataset][row][4]+AllData[dataset][row][5]*AllData[dataset][row][5]

    AverageData[row][0]=sum0/n
    AverageData[row][1]=sum2/n

# GET VALUES FOR PLOTTING
x = AverageData[:,0]
c2 = AverageData[:,1]
v22=np.sqrt(c2)

# PLOT

plt.errorbar(x,v22,yerr=0,fmt='o',label="$v_2\{2\}$")
#plotv22fromcn('DATA/test/Averagecn.txt','v','--','$v_2\{2\}$',1.0)

#plotv22fromcn('Averagecn.txt','s','-','$v_2\{2\}$',1.0)

#plotv22fromcn('BS/Averagecn.txt','v','--','$v_2\{2\}$',1.0)

plt.figure(1)
plt.legend(frameon=False,loc='upper right',numpoints=1)
plt.xlabel('$k$')
plt.show()

# HISTOGRAM MULTPLICITIES

# the histogram of the data
#n, bins, patches = plt.hist(multData, 60, normed=1, facecolor='green', alpha=0.75)
##print(bins)


#plt.xlim([min(multData), max(multData)])
#plt.hist(multData, bins=bins, normed=True, alpha=0.5)
#plt.title('pA')
#plt.xlabel('Multiplicty')
#plt.ylabel('Counts')

#plt.yscale('log')

#plt.show()
