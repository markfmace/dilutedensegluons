FOLDER="V22TEST_2"
for FILE in "F2Hist" "F4Hist"
do

cd $FOLDER
#REMOVE TARGET
rm Average${FILE}.txt

awk '{a[FNR]+=$1;b[FNR]+=$2;b2[FNR]+=$2*$2;c[FNR]+=$3;c2[FNR]+=$3*$3;d[FNR]+=$4;d2[FNR]+=$4*$4;n[FNR]++;}END{for(i=1;i<=FNR;i++) printf "%g %.16g %.16g %.16g %.16g %.16g %.16g\n", a[i]/n[i],b[i]/n[i],sqrt(n[i]*b2[i]-b[i]*b[i])/(n[i]*sqrt(n[i]-1)),c[i]/n[i],sqrt(n[i]*c2[i]-c[i]*c[i])/(n[i]*sqrt(n[i]-1)),d[i]/n[i],sqrt(n[i]*d2[i]-d[i]*d[i])/(n[i]*sqrt(n[i]-1));}' ${FILE}ID*  | tail -n +4 > Average${FILE}.txt

rm FIXEDFILES/*
mkdir -p FIXEDFILES
rm v2${FILE}.txt

for ORIG in `ls ${FILE}*`
do
tail -n +4 ${ORIG} > FIXEDFILES/${ORIG}
done

cd FIXEDFILES

awk '{a[FNR]+=$1;b[FNR]+=($3*$3+$4*$4)/($2*$2);b2[FNR]+=(($3*$3+$4*$4)/($2*$2))**2.0;c[FNR]+=($3*$3+$4*$4);c2[FNR]+=($3*$3+$4*$4)**2.0;d[FNR]+=($2*$2);d2[FNR]+=($2*$2)**2.0;n[FNR]++;}END{for(i=1;i<=FNR;i++) printf "%g %.16g %.16g %.16g %.16g %.16g %.16g\n", a[i]/n[i],b[i]/n[i],sqrt(n[i]*b2[i]-b[i]*b[i])/(n[i]*sqrt(n[i]-1)),c[i]/n[i],sqrt(n[i]*c2[i]-c[i]*c[i])/(n[i]*sqrt(n[i]-1)),d[i]/n[i],sqrt(n[i]*d2[i]-d[i]*d[i])/(n[i]*sqrt(n[i]-1));}' ${FILE}ID* > ../v2${FILE}.txt

cd ..

cd ..

done
