import numpy as np
import math
from matplotlib import pyplot as plt

#the histogram of the data
multData=np.genfromtxt("AllMultMV.txt")
multData=multData*(32*32)

weights = np.ones_like(multData)/float(len(multData))
n, bins, patches = plt.hist(multData, 100, weights=weights,facecolor='green')

plt.xlim([min(multData), max(multData)])

plt.title('pA')
plt.xlabel('dN/dy')
plt.ylabel('P(dN/dy)')

plt.yscale('log')

plt.show()
