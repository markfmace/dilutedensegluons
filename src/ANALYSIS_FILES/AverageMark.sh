FOLDER="N512a00625NRap100g2muP05g2muT1RUN1"

cd $FOLDER

#REMOVE TARGET
rm Average.txt

cat V_SingleInclusiveID* | awk '{ {S[$1]= S[$1] +$1; S0[$1]= S0[$1] +$2; S1[$1]= S1[$1] +$3*$3 + $4*$4; S2[$1]= S2[$1] +$5*$5 + $6*$6; S3[$1]= S3[$1] +$7*$7 + $8*$8; N[$1]=N[$1]+1; }} END{ for(i in S) {print i, sqrt(S1[i]/N[i]), sqrt(S2[i]/N[i]),sqrt(S3[i]/N[i]),S0[i]/N[i],S2[i]}}' | sort -g > Average.txt

cd ..
