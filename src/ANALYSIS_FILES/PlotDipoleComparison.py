import math
import numpy as np
import matplotlib.pyplot as plt

params = {'text.usetex': False, 'mathtext.fontset': 'cm'}
plt.rcParams.update(params)


def dipoleMark(f,mkr,ls,mylabel,scale):
    x = np.genfromtxt(f, usecols=(0))
    S = np.genfromtxt(f, usecols=(1))
    dS = np.genfromtxt(f, usecols=(2))
    plt.errorbar(x*scale,S,yerr=dS,marker=mkr,fmt='o',linestyle=ls,label=mylabel)

def dipoleVladi(f,mkr,ls,mylabel,scale):
    x = np.genfromtxt(f, usecols=(0))
    S = np.genfromtxt(f, usecols=(1))
    plt.errorbar(x*scale,S,yerr=0,marker=mkr,fmt='o',linestyle=ls,label=mylabel)



font = {'family': 'serif',
    'weight': 'normal',
        'size': 15,
        }

plt.rc('font', **font)
plt.rcParams.update({'figure.autolayout': True})

plt.figure(1)

#plt.xlim([10,200])
plt.ylim([0,1.0])
#ax.set_xscale("log", nonposx='clip')
#ax.set_yscale("log", nonposy='clip')


dipoleMark('TEST/AverageDipole.txt','v','--','Mark',1.0)
dipoleVladi('test_vladi.dat','s',':','Vladi',1.0)

plt.legend(frameon=False,loc='lower left',numpoints=1)
plt.xlabel('$|\mathbf{r}|$')
plt.ylabel('$S(\mathbf{r})$')


plt.savefig('DipoleComparison.pdf')

plt.show()

