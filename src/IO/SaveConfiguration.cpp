// SaveConfiguration.cpp is part of Dilute-Dense Solver
// Copyright (C) 2019 M. Mace

namespace IO{
    
    void SaveConfigurationE(std::string Efname,VectorFields *E){
        
        // OUTPUT STREAMS //
        std::ofstream EOutStream;
        
        // OUTPUT FILES //
        std::string EOutputFile=StringManipulation::StringCast(IO::OutputDirectory,Efname,"ID",RandomNumberGenerator::MySEED,".txt");
        
        // OPEN FILES //
        EOutStream.open(EOutputFile.c_str());
        
        // SET PRECISION //
        EOutStream.precision(OUTPUT_PRECISION);
        
        // CREATE OUTPUT //
        for(INT y=0;y<E->N[1];y++){
            for(INT x=0;x<E->N[0];x++){
                
                // OUTPUT ELECTRIC FIELDS //
                EOutStream << x << " " << y;
                
                EOutStream << " " << E->Get(x,y,0,0)[0];
                
                EOutStream << std::endl;
                                    
            }
        }
        
        // CLOSE OUTPUT STREAM //
        EOutStream.close();
        
    }

    
    
}
