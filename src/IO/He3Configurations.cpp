// He3Configurations.cpp is part of Dilute-Dense Solver
// Copyright (C) 2019 M. Mace

namespace IO{
    
    DOUBLE* He3Table;
    
    INT NumberOfConfigsHe3=13699;
    
    void InitializeHe3Table(){
    
        He3Table=new DOUBLE[9*NumberOfConfigsHe3];
    }
    
    void DeleteHe3Table(){
    
        delete[] He3Table;
    }
    
    INT He3TableIndex(INT PartNum,INT Config){
        
        return PartNum+9*Config;
    
    }
    
    void LoadHe3Configurations(){
        
        //NUMBER OF LINES READ FROM FILE
        INT InputCount=0;
        
        // INPUT STREAMS //
        std::ifstream InStream,EInStream;
        
        // INPUT FILES //
        std::string InputFile="src/MISC/he3.dat";
        
        // OPEN FILES //
        InStream.open(InputFile.c_str());
        
        // SET PRECISION //
        InStream.precision(OUTPUT_PRECISION);
        
        // INPUT DATA //
        DOUBLE n1x; DOUBLE n1y; DOUBLE n1z;
        DOUBLE n2x; DOUBLE n2y; DOUBLE n2z;
        DOUBLE n3x; DOUBLE n3y; DOUBLE n3z;
        
        // COMMANDLINE OUTPUT //
        std::cerr << "#READING IN He3 POSITIONS FROM FILE " << InputFile << std::endl;
        
        // INPUT GAUGE LINKS //
        if (InStream.is_open()){
            while(!InStream.eof()){
                
                // INPUT GAUGE LINKS //
                InStream >> n1x >> n1y >> n1z >> n2x >> n2y >> n2z >> n3x >> n3y >> n3z;
  
                He3Table[He3TableIndex(0,InputCount)]=n1x; He3Table[He3TableIndex(1,InputCount)]=n1y; He3Table[He3TableIndex(2,InputCount)]=n1z;
                He3Table[He3TableIndex(3,InputCount)]=n2x; He3Table[He3TableIndex(4,InputCount)]=n2y; He3Table[He3TableIndex(5,InputCount)]=n2z;
                He3Table[He3TableIndex(6,InputCount)]=n3x; He3Table[He3TableIndex(7,InputCount)]=n3y; He3Table[He3TableIndex(8,InputCount)]=n3z;
                
                //INCREASE POSITION COUNT
                InputCount++;
                
            }
        }

        if(InputCount-1 != NumberOfConfigsHe3){
            std::cerr << "#He3 TABLE NOT FILLED CORRECTLY " << InputCount << " " << NumberOfConfigsHe3 << std::endl;
            exit(0);
        }
        
        // COMMANDLINE OUTPUT //
        std::cerr << "#FINISHED READING IN He3 TABLE FROM FILE " << InputFile << std::endl;
        
    }
    
}


