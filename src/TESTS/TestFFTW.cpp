// TestFFTW.cpp is part of Dilute-Dense Solver
// Copyright (C) 2019 M. Mace

#ifndef __TEST_FFTW_CPP__
#define __TEST_FFTW_CPP__

// SERVES AS TEST SUCH THAT WHEN ONE PERFORMS P->X, MUST RE-NORMALIZE BY 1.0/(Lattice::N[0]*Lattice::N[1]) //
// HOWEVER NO FURTHER NORMALIZATION IS NEEDED FOR X->P //

void TestFFTW(WilsonLines* U){
    
    std::cerr << "# STARTING FOURIER TRANSFORM TEST" << std::endl;
    
    // INITIAL X-SPACE VALUES //
    std::ofstream XInitOutStream; XInitOutStream.open(StringManipulation::StringCast(IO::OutputDirectory,"XInitID",MY_MPI_RNG_SEED,".txt").c_str());
    std::ofstream PInitOutStream; PInitOutStream.open(StringManipulation::StringCast(IO::OutputDirectory,"PInitID",MY_MPI_RNG_SEED,".txt").c_str());

    std::ofstream PTranfsOutStream; PTranfsOutStream.open(StringManipulation::StringCast(IO::OutputDirectory,"PTranfsID",MY_MPI_RNG_SEED,".txt").c_str());
    std::ofstream XTranfsOutStream; XTranfsOutStream.open(StringManipulation::StringCast(IO::OutputDirectory,"XTranfsID",MY_MPI_RNG_SEED,".txt").c_str());
    
    std::ofstream PFinalOutStream; PFinalOutStream.open(StringManipulation::StringCast(IO::OutputDirectory,"PFinalID",MY_MPI_RNG_SEED,".txt").c_str());
    std::ofstream XFinalOutStream; XFinalOutStream.open(StringManipulation::StringCast(IO::OutputDirectory,"XFinalID",MY_MPI_RNG_SEED,".txt").c_str());

    // SET RHO FIELDS IN X SPACE //
    for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
        for(INT y=0;y<U->N[1];y++){
            for(INT x=0;x<U->N[0];x++){
                
                COMPLEX LocalValue=RandomNumberGenerator::ComplexGauss();
                
                // TAKE IN COMPLEX RANDOM VALUE IN x-SPACE //
                FourierSpace::RhoT->SetXc(x,y,a,LocalValue);
                // TAKE IN COMPLEX RANDOM VALUE IN p-SPACE //
                FourierSpace::RhoP->SetP(x,y,a,LocalValue);

                // OUTPUT TO FILE //
                XInitOutStream << x << " " << y << " " << a << " " << real(FourierSpace::RhoT->GetX(x,y,a)) << " " << imag(FourierSpace::RhoT->GetX(x,y,a)) << std::endl;
                PInitOutStream << x << " " << y << " " << a << " " << real(FourierSpace::RhoP->GetP(x,y,a)) << " " << imag(FourierSpace::RhoP->GetP(x,y,a)) << std::endl;  // TECHNICALLY THIS IS pXIndex, pYIndex //

            }
        }
        
    }

    std::cerr << "# INITIAL FIELDS SAVED" << std::endl;


    // FOURIER TRANSFORM RhoT TO MOMENTUM SPACE //
    FourierSpace::RhoT->ExecuteXtoP();
    
    // FOURIER TRANSFORM RhoT TO MOMENTUM SPACE //
    FourierSpace::RhoP->ExecutePtoX();
    
    std::cerr << "# FIRST FOURIER TRANSFORM PERFORMED" << std::endl;

    // OUTPUT
    //#pragma omp parallel for collapse(2)
    for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
        for(INT pYIndex=0;pYIndex<Lattice::N[1];pYIndex++){
            for(INT pXIndex=0;pXIndex<Lattice::N[0];pXIndex++){
                
                // RENORMALIZE AFTER X->P -- [d^2x] = [a^2/hbarc^2] = GeV^-2 //
                COMPLEX RhoTTEMP=FourierSpace::RhoT->GetP(pXIndex,pYIndex,a);
                FourierSpace::RhoT->SetP(pXIndex,pYIndex,a,RhoTTEMP*COMPLEX(Lattice::a[0]*Lattice::a[1]*fmtoinvGeV*fmtoinvGeV));
                
                // RENORMALIZE AFTER P->X -- [d^2p] = [hbarc^2/(Na)^2] = GeV^2 //
                COMPLEX RhoPTEMP=FourierSpace::RhoP->GetX(pXIndex,pYIndex,a);
                FourierSpace::RhoP->SetXc(pXIndex,pYIndex,a,RhoPTEMP*invfmtoGeV*invfmtoGeV/COMPLEX(Lattice::N[0]*Lattice::N[1]*Lattice::a[0]*Lattice::a[1]));
                
                // SAVE MOMENTUM SPACE RhoT //
                PTranfsOutStream << pXIndex << " " << pYIndex << " " << a << " " << real(FourierSpace::RhoT->GetP(pXIndex,pYIndex,a)) << " " << imag(FourierSpace::RhoT->GetP(pXIndex,pYIndex,a)) << std::endl;
                // SAVE POSITION SPACE RhoP //
                XTranfsOutStream << pXIndex << " " << pYIndex << " " << a << " " << real(FourierSpace::RhoP->GetX(pXIndex,pYIndex,a)) << " " << imag(FourierSpace::RhoP->GetX(pXIndex,pYIndex,a)) << std::endl; // TECHNICALLY THIS IS x,y

            }

        }
    }
    
    std::cerr << "# SECOND FIELDS SAVED" << std::endl;
    
    // FOURIER TRANSFORM BACK //
    FourierSpace::RhoT->ExecutePtoX();
    
    // FOURIER TRANSFORM BACK //
    FourierSpace::RhoP->ExecuteXtoP();
    
    std::cerr << "# SECOND FOURIER TRANSFORM PERFORMED" << std::endl;

    // SET RHO FIELDS IN X SPACE //
    for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
        for(INT y=0;y<U->N[1];y++){
            for(INT x=0;x<U->N[0];x++){

                // RENORMALIZE AFTER P->X -- [d^2p] = [hbarc^2/(Na)^2] = GeV^2 //
                COMPLEX RhoTTEMP=FourierSpace::RhoT->GetX(x,y,a);
                FourierSpace::RhoT->SetXc(x,y,a,RhoTTEMP*invfmtoGeV*invfmtoGeV/COMPLEX(Lattice::N[0]*Lattice::N[1]*Lattice::a[0]*Lattice::a[1]));
                
                // RENORMALIZE AFTER X->P -- [d^2x] = [a^2/hbarc^2] = GeV^-2 //
                COMPLEX RhoPTEMP=FourierSpace::RhoP->GetP(x,y,a);
                FourierSpace::RhoP->SetP(x,y,a,RhoPTEMP*COMPLEX(Lattice::a[0]*Lattice::a[1]*fmtoinvGeV*fmtoinvGeV));
                
                // SAVE FINAL MOMENTUM SPACE RhoT //
                XFinalOutStream << x << " " << y << " " << a << " " << real(FourierSpace::RhoT->GetX(x,y,a)) << " " << imag(FourierSpace::RhoT->GetX(x,y,a)) << std::endl;
                // SAVE FINAL POSITION SPACE RhoP //
                PFinalOutStream << x << " " << y << " " << a << " " << real(FourierSpace::RhoP->GetP(x,y,a)) << " " << imag(FourierSpace::RhoP->GetP(x,y,a)) << std::endl;

            }
        }
        
    }
    
    std::cerr << "# FINAL FIELDS SAVED" << std::endl;

    // CLEAN-UP //
    XInitOutStream.close();
    PInitOutStream.close();
    
    PTranfsOutStream.close();
    XTranfsOutStream.close();
    
    XFinalOutStream.close();
    PFinalOutStream.close();
    
    std::cerr << "# COMPLETED FOURIER TRANSFORM TEST" << std::endl;

}

#endif
