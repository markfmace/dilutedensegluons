// THIS IS OLD AND DOESNT WORK //

namespace Tests{
    
    void NaiveDipoleQs(GaugeLinks *U){
        
        // NAIVE DIPOLE //
        // SETUP HISTOGRAM //
        DOUBLE MAX_LENGTHN=std::pow((Lattice::N[0]*Lattice::a[0])*(Lattice::N[1]*Lattice::a[1]),1.0/2.0);
        
        INT NumberOfBinsN=8*INT(MAX_LENGTHN);
        
        Histogram *NaiveDipoleHistogram=new Histogram(0.0,MAX_LENGTHN,NumberOfBinsN);
        
        for(INT y2=0;y2<=U->N[1]-1;y2++){
            for(INT x2=0;x2<=U->N[0]-1;x2++){
                for(INT y1=0;y1<=U->N[1]-1;y1++){
                    for(INT x1=0;x1<=U->N[0]-1;x1++){
                        
                        COMPLEX Buffer0[SUNcGroup::MatrixSize];
                        COMPLEX Buffer1[SUNcGroup::MatrixSize];
                        
                        // PROJECTS OUT U to U^a
                        SUNcGroup::Operations::GeneratorProjection(U->Get(x1,y1,0),Buffer0);
                        SUNcGroup::Operations::GeneratorProjection(U->Get(x2,y2,0),Buffer1);
                        COMPLEX TempSum={0.0,0.0};
                        for(INT a=0;a<SUNcGroup::MatrixSize;a++){
                            TempSum+=Buffer0[a]*conj(Buffer1[a]);
                        }
                        
                        NaiveDipoleHistogram->Count(sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)),real(TempSum)/(2.0*Nc));
                    }
                }
            }
        }
        
        // SET OUTPUT FILE//
        std::string OutputFileN=StringManipulation::StringCast(IO::OutputDirectory,"NaiveDipole","ID",RandomNumberGenerator::MySEED,".txt");
        
        
        // SET OUTPUT HEADER //
        std::string HeaderMessageN=StringManipulation::StringCast("# rAbs ","S(r)");
        
        // OUTPUT WILSON LOOP //
        NaiveDipoleHistogram->Output(HeaderMessageN,OutputFileN);
        
        // GET Qs
        DOUBLE sqrt2overQsNaive=NaiveDipoleHistogram->GetxValueDECR(exp(-0.5),U->a[0]);
        
        std::cerr << "# NAIVE DIPOLE Qs=" << sqrt(2.0)/sqrt2overQsNaive << " " << sqrt2overQsNaive << std::endl;
        
        // CLEAN-UP //
        delete NaiveDipoleHistogram;
    }
    
    void DipolePosition(GaugeLinks *U){
        
        // NAIVE DIPOLE //
        // SETUP HISTOGRAM //
        DOUBLE MAX_LENGTHN=std::pow((Lattice::N[0]*Lattice::a[0])*(Lattice::N[1]*Lattice::a[1]),1.0/2.0);
        
        INT NumberOfBinsN=8*INT(MAX_LENGTHN);
        
        Histogram *NaiveDipoleHistogram=new Histogram(0.0,MAX_LENGTHN,NumberOfBinsN);
        
        COMPLEX Buffer0[SUNcGroup::MatrixSize];
        SUNcGroup::Operations::GeneratorProjection(U->Get(0,0,0),Buffer0);
        
        for(INT y1=0;y1<=U->N[1]-1;y1++){
            for(INT x1=0;x1<=U->N[0]-1;x1++){
                
                COMPLEX Buffer1[SUNcGroup::MatrixSize];
                
                // PROJECTS OUT U to U^a
                SUNcGroup::Operations::GeneratorProjection(U->Get(x1,y1,0),Buffer1);
                COMPLEX TempSum={0.0,0.0};
                for(INT a=0;a<SUNcGroup::MatrixSize;a++){
                    TempSum+=Buffer0[a]*conj(Buffer1[a]);
                }
                
                NaiveDipoleHistogram->Count(sqrt((x1)*(x1)+(y1)*(y1)),real(TempSum)/(2.0*Nc));
            }
        }
        
        // SET OUTPUT FILE//
        std::string OutputFileN=StringManipulation::StringCast(IO::OutputDirectory,"NaiveDipole","ID",RandomNumberGenerator::MySEED,".txt");
        
        
        // SET OUTPUT HEADER //
        std::string HeaderMessageN=StringManipulation::StringCast("# rAbs ","S(r)");
        
        // OUTPUT WILSON LOOP //
        NaiveDipoleHistogram->Output(HeaderMessageN,OutputFileN);
        
        // CLEAN-UP //
        delete NaiveDipoleHistogram;
        
    }
   
    void DipolePositionMATRIXMULT(GaugeLinks *U){
        
        // NAIVE DIPOLE //
        // SETUP HISTOGRAM //
        DOUBLE MAX_LENGTHN=std::pow((Lattice::N[0]*Lattice::a[0])*(Lattice::N[1]*Lattice::a[1]),1.0/2.0);
        
        INT NumberOfBinsN=1*INT(MAX_LENGTHN);
        
        Histogram *NaiveDipoleHistogram=new Histogram(0.0,MAX_LENGTHN,NumberOfBinsN);
        
        for(INT y1=0;y1<=U->N[1]-1;y1++){
            for(INT x1=0;x1<=U->N[0]-1;x1++){
                
                SU_Nc_FUNDAMENTAL_FORMAT Buffer[SUNcGroup::MatrixSize];
                
                // PROJECTS OUT U to U^a
                //SUNcGroup::Operations::UD(U->Get(0,0,0),U->Get(x1,y1,0),Buffer);
                COMPLEX TempSum=SUNcGroup::Operations::ReTr(Buffer);
                

                NaiveDipoleHistogram->Count(sqrt((x1)*(x1)+(y1)*(y1)),real(TempSum)/(Nc));
            }
        }
        
        // SET OUTPUT FILE//
        std::string OutputFileN=StringManipulation::StringCast(IO::OutputDirectory,"NaiveDipole","ID",RandomNumberGenerator::MySEED,".txt");
        
        
        // SET OUTPUT HEADER //
        std::string HeaderMessageN=StringManipulation::StringCast("# rAbs ","S(r)");
        
        // OUTPUT WILSON LOOP //
        NaiveDipoleHistogram->Output(HeaderMessageN,OutputFileN);
        
        // CLEAN-UP //
        delete NaiveDipoleHistogram;
        
    }

}
