// SingleParticleDistribution.cpp is part of Dilute-Dense Solver
// Copyright (C) 2019 M. Mace

#ifndef __SINGLEPARTICLEDISTRIBUTION__CPP__
#define __SINGLEPARTICLEDISTRIBUTION__CPP__

namespace Observables{
    
    // AVOID DIVERGENCES IF USING LATTICE REGULATOR //
    double UV=1e-9;

    // ENFORCE PERIODIC BOUNDARIES //
    INT BoundaryIndex(INT i){
        if(i<0){return i+Lattice::N[0];}
        else if(i>Lattice::N[0]-1){return i-Lattice::N[0];}
        else{return i;}
    }
    
    INT SIGN_V(DOUBLE i){
        if(i<0){return -1;}
        else{return 1;}
    }

    // GENERAL PURPOSE FUNCTIONS FOR CONVERTING INDICES TO MOMENTA -- SHOULD MOVE TO MOMENTA FILE //
    // IN GeV
    DOUBLE IndexTok(INT Index){
        DOUBLE karg=2.0*PI*Index/(Lattice::N[0]*Lattice::a[0]);
        DOUBLE kValue=1.0/Lattice::a[0]*sin(karg*Lattice::a[0])*invfmtoGeV;
        return kValue;
    }
    // IN GeV^2 //
    DOUBLE IndexTokSqr(INT kXIndex,INT kYIndex){
        DOUBLE kXarg=2.0*PI*kXIndex/(Lattice::N[0]*Lattice::a[0]);
        DOUBLE kYarg=2.0*PI*kYIndex/(Lattice::N[1]*Lattice::a[1]);
        return (pow(2.0/Lattice::a[0]*sin(kXarg*Lattice::a[0]/2.0),2)+pow(2.0/Lattice::a[0]*sin(kYarg*Lattice::a[1]/2.0),2))*invfmtoGeV*invfmtoGeV;
    }

    // CONSTRUCT OMEGA_S AND OMEGA_A //
    void ConstructOmegas(CVectorFields *OmS,CVectorFields *OmA){

        // DETERMINE DERIVATES AND OMEGAS
        //#pragma omp for collapse(2)
        for(INT x=0;x<Lattice::N[0];x++){
            for(INT y=0;y<Lattice::N[1];y++){

                SU_Nc_FUNDAMENTAL_FORMAT OmegaSBuffer[SUNcGroup::MatrixSize]={SU_Nc_FUNDAMENTAL_FORMAT(0.0),SU_Nc_FUNDAMENTAL_FORMAT(0.0),SU_Nc_FUNDAMENTAL_FORMAT(0.0),SU_Nc_FUNDAMENTAL_FORMAT(0.0),SU_Nc_FUNDAMENTAL_FORMAT(0.0),SU_Nc_FUNDAMENTAL_FORMAT(0.0),SU_Nc_FUNDAMENTAL_FORMAT(0.0),SU_Nc_FUNDAMENTAL_FORMAT(0.0),SU_Nc_FUNDAMENTAL_FORMAT(0.0)};
                SU_Nc_FUNDAMENTAL_FORMAT OmegaABuffer[SUNcGroup::MatrixSize]={SU_Nc_FUNDAMENTAL_FORMAT(0.0),SU_Nc_FUNDAMENTAL_FORMAT(0.0),SU_Nc_FUNDAMENTAL_FORMAT(0.0),SU_Nc_FUNDAMENTAL_FORMAT(0.0),SU_Nc_FUNDAMENTAL_FORMAT(0.0),SU_Nc_FUNDAMENTAL_FORMAT(0.0),SU_Nc_FUNDAMENTAL_FORMAT(0.0),SU_Nc_FUNDAMENTAL_FORMAT(0.0),SU_Nc_FUNDAMENTAL_FORMAT(0.0)};
                
                COMPLEX CHECK=COMPLEX(0.0,0.0);

                for(INT a=0;a<SUNcAlgebra::VectorSize;a++){

                    // PROJECTILE DERIVATIVES //
                    // (DIMLESS)^2/fm -> GeV //
                    double dUdxProj=0.5*(ProjSolution::A->Get(BoundaryIndex(x+1),y,0,a)[0]-ProjSolution::A->Get(BoundaryIndex(x-1),y,0,a)[0])/Lattice::a[0]*invfmtoGeV;
                    double dUdyProj=0.5*(ProjSolution::A->Get(x,BoundaryIndex(y+1),0,a)[0]-ProjSolution::A->Get(x,BoundaryIndex(y-1),0,a)[0])/Lattice::a[1]*invfmtoGeV;

                    SU_Nc_FUNDAMENTAL_FORMAT Wx[SUNcGroup::MatrixSize];
                    SU_Nc_FUNDAMENTAL_FORMAT Wy[SUNcGroup::MatrixSize];

                    // BUFFER //
                    SU_Nc_FUNDAMENTAL_FORMAT Buffer[SUNcGroup::MatrixSize];

                    // LAMBDA BUFFER //
                    SU_Nc_FUNDAMENTAL_FORMAT Generator[SUNcGroup::MatrixSize];
                    COPY_SUNcMatrix(Generator,SUNcAlgebra::GeneratorTa[a]);

                    // U^dag t^a U at x+1 //
                    SU_Nc_FUNDAMENTAL_FORMAT tmpPlus[SUNcGroup::MatrixSize];
                    COPY_SUNcMatrix(tmpPlus,TargetFields::U->Get(BoundaryIndex(x+1),y,0));

                    SUNcGroup::Operations::UU(Generator,tmpPlus,Buffer);

                    SUNcGroup::Operations::DU(TargetFields::U->Get(BoundaryIndex(x+1),y,0),Buffer,tmpPlus);

                    // U^dag t^a U at x-1 //
                    SU_Nc_FUNDAMENTAL_FORMAT tmpMinus[SUNcGroup::MatrixSize];
                    COPY_SUNcMatrix(tmpMinus,TargetFields::U->Get(BoundaryIndex(x-1),y,0));

                    SUNcGroup::Operations::UU(Generator,tmpMinus,Buffer);

                    SUNcGroup::Operations::DU(TargetFields::U->Get(BoundaryIndex(x-1),y,0),Buffer,tmpMinus);

                    for(INT alpha=0;alpha<Nc;alpha++){
                        for(INT beta=0;beta<Nc;beta++){
                            // GeV //
                            Wx[SUNcGroup::MatrixIndex(alpha,beta)]=DOUBLE(0.5)/Lattice::a[0]*(tmpPlus[SUNcGroup::MatrixIndex(alpha,beta)]-tmpMinus[SUNcGroup::MatrixIndex(alpha,beta)])*invfmtoGeV;
                        }
                    }

                    // U^dag t^a U at y+1//
                    COPY_SUNcMatrix(tmpPlus,TargetFields::U->Get(x,BoundaryIndex(y+1),0));

                    SUNcGroup::Operations::UU(Generator,tmpPlus,Buffer);

                    SUNcGroup::Operations::DU(TargetFields::U->Get(x,BoundaryIndex(y+1),0),Buffer,tmpPlus);

                    // U^dag t^a U at y-1//
                    COPY_SUNcMatrix(tmpMinus,TargetFields::U->Get(x,BoundaryIndex(y-1),0));

                    SUNcGroup::Operations::UU(Generator,tmpMinus,Buffer);

                    SUNcGroup::Operations::DU(TargetFields::U->Get(x,BoundaryIndex(y-1),0),Buffer,tmpMinus);

                    for(INT alpha=0;alpha<Nc;alpha++){
                        for(INT beta=0;beta<Nc;beta++){
                            // GeV //
                            Wy[SUNcGroup::MatrixIndex(alpha,beta)]=DOUBLE(0.5)/Lattice::a[1]*(tmpPlus[SUNcGroup::MatrixIndex(alpha,beta)]-tmpMinus[SUNcGroup::MatrixIndex(alpha,beta)])*invfmtoGeV;
                        }
                    }

                    CHECK+=(OmegaSBuffer[SUNcGroup::MatrixIndex(0,0)]);

                    // SET OMEGA MATRICES //
                    for(INT alpha=0;alpha<Nc;alpha++){
                        for(INT beta=0;beta<Nc;beta++){
                            // GeV^2 //
                            OmegaSBuffer[SUNcGroup::MatrixIndex(alpha,beta)]+=dUdxProj*Wx[SUNcGroup::MatrixIndex(alpha,beta)]+dUdyProj*Wy[SUNcGroup::MatrixIndex(alpha,beta)];
                            OmegaABuffer[SUNcGroup::MatrixIndex(alpha,beta)]+=dUdxProj*Wy[SUNcGroup::MatrixIndex(alpha,beta)]-dUdyProj*Wx[SUNcGroup::MatrixIndex(alpha,beta)];
                        }
                    }
                    
                } // END VECTORSIZE a LOOP//

                COMPLEX OmegaSgenBuffer[SUNcGroup::MatrixSize];
                COMPLEX OmegaAgenBuffer[SUNcGroup::MatrixSize];

                SUNcGroup::Operations::GeneratorProjection(OmegaSBuffer,OmegaSgenBuffer);
                SUNcGroup::Operations::GeneratorProjection(OmegaABuffer,OmegaAgenBuffer);

                // SET TO VECTOR //
                for(INT a=0;a<SUNcGroup::MatrixSize;a++){

                    OmS->Get(x,y,0,a)[0]=OmegaSgenBuffer[a]; // GeV^2
                    OmA->Get(x,y,0,a)[0]=OmegaAgenBuffer[a]; // GeV^2

                }

            } // END y LOOP //
        } // END x LOOP //

    }

    COMPLEX EvenSingleInclusive(INT kXIndex,INT kYIndex){

        COMPLEX SIloc=COMPLEX(0.0,0.0);

        // SUM OVER ALEGBRA INDICES OF |OMEGA_S|^2+|OMEGA_A|^2 TO GET SINGLE INCLUSIVE//
        for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
            // (DIMLESS)^2 //
            SIloc+=FourierSpace::OmegaS->GetP(BoundaryIndex(Lattice::N[0]-kXIndex),BoundaryIndex(Lattice::N[1]-kYIndex),a)*FourierSpace::OmegaS->GetP(kXIndex,kYIndex,a)+FourierSpace::OmegaA->GetP(BoundaryIndex(Lattice::N[0]-kXIndex),BoundaryIndex(Lattice::N[1]-kYIndex),a)*FourierSpace::OmegaA->GetP(kXIndex,kYIndex,a);
        }

        // RE-NORMALIZE 1/(k^2 (2\pi)^3 L^2) //
        // GeV^2//
        DOUBLE kSqr=IndexTokSqr(kXIndex,kYIndex);
        
        // (DIMLESS)^2 /(GeV^2) //
        SIloc*=1.0/((kSqr+RegMass*RegMass+UV)*pow(2.0*PI,3)); // GeV^-2 //

        return SIloc;

    }

    // COMPLETE ANTISYMMETRIC SINGLE INCLUSIVE -- BASED ON arXiv:1802.08166 EQN D5 //
    COMPLEX OddSingleInclusive(INT kXIndex, INT kYIndex){
        
        // GeV, GeV, GeV^2 //
        DOUBLE kXValue=IndexTok(kXIndex);
        DOUBLE kYValue=IndexTok(kYIndex);
        DOUBLE kSqr=IndexTokSqr(kXIndex,kYIndex);
        
        COMPLEX Integrate=COMPLEX(0.0,0.0);
        
        // q-INTEGRATION //
        for(INT qXIndex=0;qXIndex<Lattice::N[0];qXIndex++){
            for(INT qYIndex=0;qYIndex<Lattice::N[1];qYIndex++){
                
                INT kXMinusqXIndex=BoundaryIndex(kXIndex-qXIndex);
                INT kYMinusqYIndex=BoundaryIndex(kYIndex-qYIndex);
                // q_x, q_y //
                DOUBLE qXValue=IndexTok(qXIndex);
                DOUBLE qYValue=IndexTok(qYIndex);
                // q^2 -- GeV^2 //
                DOUBLE qSqr=IndexTokSqr(qXIndex,qYIndex);
                // (k-q)_i -- GeV //
                DOUBLE kXMinusqX=IndexTok(kXMinusqXIndex);
                DOUBLE kYMinusqY=IndexTok(kYMinusqYIndex);
                // k.(k-q) -- GeV^2 //
                DOUBLE kDotkMinusq=kXValue*kXMinusqX+kYValue*kYMinusqY;
                // q.(k-q) -- GeV^2 //
                DOUBLE qDotkMinusq=qXValue*kXMinusqX+qYValue*kYMinusqY;
                // (k-q)^2 -- GeV^2 //
                DOUBLE kMinusqSqr=IndexTokSqr(kXMinusqXIndex,kYMinusqYIndex);
                
                // CROSS PRODUCT -- GeV^2 //
                DOUBLE kCrossq=kXMinusqX*qYValue-kYMinusqY*qXValue;
                
                COMPLEX Sum=COMPLEX(0.0,0.0);
                
                for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                    for(INT b=0;b<SUNcAlgebra::VectorSize;b++){
                        for(INT c=0;c<SUNcAlgebra::VectorSize;c++){
                            if(SUNcAlgebra::StructureFunctions::f0(a,b,c)*SUNcAlgebra::StructureFunctions::f0(a,b,c)>0){
                                
                                 //INTEGRATION -- INHERENTLY GETTING IMAGINARY PART //
                                // (DIMLESS)/((GeV^2)*(GeV^2))*(GeV^2*DIMLESS^3)=GeV^-2 //
                                Sum+=SUNcAlgebra::StructureFunctions::f0(a,b,c)*SIGN_V(kCrossq)*1.0/((qSqr+UV)*(kMinusqSqr+UV))*(
                                    (kSqr*FourierSpace::OmegaA->GetP(qXIndex,qYIndex,a)*FourierSpace::OmegaA->GetP(kXMinusqXIndex,kYMinusqYIndex,b)*FourierSpace::OmegaA->GetP(BoundaryIndex(Lattice::N[0]-kXIndex),BoundaryIndex(Lattice::N[1]-kYIndex),c)
                                     -qDotkMinusq*FourierSpace::OmegaA->GetP(qXIndex,qYIndex,a)*FourierSpace::OmegaA->GetP(kXMinusqXIndex,kYMinusqYIndex,b)*FourierSpace::OmegaA->GetP(BoundaryIndex(Lattice::N[0]-kXIndex),BoundaryIndex(Lattice::N[1]-kYIndex),c)
                                     -qDotkMinusq*FourierSpace::OmegaS->GetP(qXIndex,qYIndex,a)*FourierSpace::OmegaS->GetP(kXMinusqXIndex,kYMinusqYIndex,b)*FourierSpace::OmegaA->GetP(BoundaryIndex(Lattice::N[0]-kXIndex),BoundaryIndex(Lattice::N[1]-kYIndex),c)
                                     +2.0*kDotkMinusq*FourierSpace::OmegaA->GetP(qXIndex,qYIndex,a)*FourierSpace::OmegaS->GetP(kXMinusqXIndex,kYMinusqYIndex,b)*FourierSpace::OmegaS->GetP(BoundaryIndex(Lattice::N[0]-kXIndex),BoundaryIndex(Lattice::N[1]-kYIndex),c))
                                    -(kSqr*FourierSpace::OmegaA->GetP(BoundaryIndex(Lattice::N[0]-qXIndex),BoundaryIndex(Lattice::N[1]-qYIndex),a)*FourierSpace::OmegaA->GetP(BoundaryIndex(Lattice::N[0]-kXMinusqXIndex),BoundaryIndex(Lattice::N[1]-kYMinusqYIndex),b)
                                          *FourierSpace::OmegaA->GetP(kXIndex,kYIndex,c)
                                      -qDotkMinusq*FourierSpace::OmegaA->GetP(BoundaryIndex(Lattice::N[0]-qXIndex),BoundaryIndex(Lattice::N[1]-qYIndex),a)*FourierSpace::OmegaA->GetP(BoundaryIndex(Lattice::N[0]-kXMinusqXIndex),BoundaryIndex(Lattice::N[1]-kYMinusqYIndex),b)*FourierSpace::OmegaA->GetP(kXIndex,kYIndex,c)
                                      -qDotkMinusq*FourierSpace::OmegaS->GetP(BoundaryIndex(Lattice::N[0]-qXIndex),BoundaryIndex(Lattice::N[1]-qYIndex),a)*FourierSpace::OmegaS->GetP(BoundaryIndex(Lattice::N[0]-kXMinusqXIndex),BoundaryIndex(Lattice::N[1]-kYMinusqYIndex),b)*FourierSpace::OmegaA->GetP(kXIndex,kYIndex,c)
                                      +2.0*kDotkMinusq*FourierSpace::OmegaA->GetP(BoundaryIndex(Lattice::N[0]-qXIndex),BoundaryIndex(Lattice::N[1]-qYIndex),a)*FourierSpace::OmegaS->GetP(BoundaryIndex(Lattice::N[0]-kXMinusqXIndex),BoundaryIndex(Lattice::N[1]-kYMinusqYIndex),b)*FourierSpace::OmegaS->GetP(kXIndex,kYIndex,c))
                                    );

                            }// END IF |f|>0 STATEMENT //

                        }// END c LOOP ///
                        //Integrate+=Sum;
                        
                    }// END b LOOP //
                }// END a LOOP //
                Integrate+=Sum;
            } // END qX LOOP //
        } // END qY LOOP //
        
        // (GeV)^-2 \int d^2p (GeV^-2) -> GeV^-2 //
        return ComplexI*0.5*Integrate/((kSqr+RegMass*RegMass+UV)*std::pow(2.0*PI,3))*(invfmtoGeV*invfmtoGeV/(Lattice::N[0]*Lattice::a[0]*Lattice::N[1]*Lattice::a[1]));

    }

    // SINGLE PARTICLE SPECTRA //
    void DetermineSingleParticleDistribution(CVectorFields *OmS,CVectorFields *OmA){

        // INITIALIZE FOURIER TRANSFORM VARIABLES //
        //#pragma omp for collapse(2)
        for(INT y=0;y<Lattice::N[1];y++){
            for(INT x=0;x<Lattice::N[0];x++){
                for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                    // [OmS]=GeV^2 //
                    FourierSpace::OmegaS->SetXc(x,y,a,OmS->Get(x,y,0,a)[0]);
                    FourierSpace::OmegaA->SetXc(x,y,a,OmA->Get(x,y,0,a)[0]);

                }
            }
        }

        // COMPLETE FOURIER TRANSFORMs -- NO FFTW NORMALIZATION NEEDED  //
        FourierSpace::OmegaS->ExecuteXtoP();
        FourierSpace::OmegaA->ExecuteXtoP();
        
        // RENORMALIZE AFTER X->P -- [d^2x] = [a^2/hbarc^2] = GeV^-2 //
        for(INT y=0;y<Lattice::N[1];y++){
            for(INT x=0;x<Lattice::N[0];x++){
                for(INT a=0;a<SUNcAlgebra::VectorSize;a++){
                    
                    COMPLEX OmegaSTEMP=FourierSpace::OmegaS->GetP(x,y,a); // GeV^2 //
                    FourierSpace::OmegaS->SetP(x,y,a,OmegaSTEMP*COMPLEX(Lattice::a[0]*Lattice::a[1]*fmtoinvGeV*fmtoinvGeV)); // DIMLESS //
                    
                    COMPLEX OmegaATEMP=FourierSpace::OmegaA->GetP(x,y,a); // GeV^2 //
                    FourierSpace::OmegaA->SetP(x,y,a,OmegaATEMP*COMPLEX(Lattice::a[0]*Lattice::a[1]*fmtoinvGeV*fmtoinvGeV)); // DIMLESS //
                    
                }
            }
        }

        // CREATE OUTPUT STREAM FOR SINGLE INCLUSIVE DISTRIBUTION //
        std::ofstream VNOutStream; VNOutStream.open(StringManipulation::StringCast(IO::OutputDirectory,"HarmonicID",MY_MPI_RNG_SEED,".txt").c_str());
        
        INT kMaxIndex=INT((kMax-kMin)/dkStep+0.1);
        
        // REFERENCE Vn //
        DOUBLE v0REF=0.0;
        COMPLEX v1REF={0.0,0.0};
        COMPLEX v2REF={0.0,0.0};
        COMPLEX v3REF={0.0,0.0};
        COMPLEX v4REF={0.0,0.0};
        // BIN COUNTER //
        INT NcountREF=0;
        
        std::cerr << "# BEGINNING REFERENCE BIN CALCULATION " << std::endl;
        
        // REFERENCE BIN //
        // LOOP OVER ALL LATTICE MOMENTA FOR ANGULAR INTEGRATION -- RESTRICTING FOR CERTAIN KEY BANDS //
        for(INT k2XIndex=0;k2XIndex<Lattice::N[0];k2XIndex++){
            if((k2XIndex<Lattice::N[0]/4+1)||(k2XIndex>3*Lattice::N[0]/4-1)){
                for(INT k2YIndex=0;k2YIndex<Lattice::N[1];k2YIndex++){
                    if((k2YIndex<Lattice::N[1]/4+1)||(k2YIndex>3*Lattice::N[0]/4-1)){
                        
                        // PHYSICAL MOMENTUM VALUES -- GeV //
                        DOUBLE k2XValue=IndexTok(k2XIndex);
                        DOUBLE k2YValue=IndexTok(k2YIndex);
                        
                        // DEFINE k^2 AND |k| -- GeV^2, GeV //
                        DOUBLE k2Sqr=IndexTokSqr(k2XIndex,k2YIndex);
                        DOUBLE k2Abs=sqrt(k2Sqr);
                        // BIN BY MOMENTUM SPACING //
                        
                        // SINGLE INCLUSIVE BUFFER -- GeV^-2 //
                        COMPLEX SIloc=COMPLEX(0.0);
                        COMPLEX AsSIloc=COMPLEX(0.0,0.0);
                        
                        // DEFINE AZIMUTHAL ANGLE -- DIMLESS //
                        DOUBLE Phik2=atan2(k2YValue,k2XValue);
                        
                        if(k2Abs>=kREFMin && k2Abs<=kREFMax){
                            
                            SIloc=EvenSingleInclusive(k2XIndex,k2YIndex);
                            
                            // ASSIGN IF DESIGNATED //
                            if(AS_FLAG==1){
                                AsSIloc=OddSingleInclusive(k2XIndex,k2YIndex);
                            }
                            
                            // COMPUTE v_n{1}'s //
                            //SI+=real(SIloc);
                            //ASI+=real(AsSIloc);
                            
                            if(kINT_FLAG==0){
                                v0REF+=real(k2Abs*dkBin*SIloc);
                                v1REF+=k2Abs*dkBin*AsSIloc*exp(1.0*ComplexI*Phik2);
                                v2REF+=k2Abs*dkBin*SIloc*exp(2.0*ComplexI*Phik2);
                                v3REF+=k2Abs*dkBin*AsSIloc*exp(3.0*ComplexI*Phik2);
                                v4REF+=k2Abs*dkBin*SIloc*exp(4.0*ComplexI*Phik2);
                            }
                            else if(kINT_FLAG==1){
                                v0REF+=real(k2Abs*SIloc);
                                v1REF+=k2Abs*AsSIloc*exp(1.0*ComplexI*Phik2);
                                v2REF+=k2Abs*SIloc*exp(2.0*ComplexI*Phik2);
                                v3REF+=k2Abs*AsSIloc*exp(3.0*ComplexI*Phik2);
                                v4REF+=k2Abs*SIloc*exp(4.0*ComplexI*Phik2);
                            }
                            else if(kINT_FLAG==2){
                                v0REF+=real(SIloc);
                                v1REF+=AsSIloc*exp(1.0*ComplexI*Phik2);
                                v2REF+=SIloc*exp(2.0*ComplexI*Phik2);
                                v3REF+=AsSIloc*exp(3.0*ComplexI*Phik2);
                                v4REF+=SIloc*exp(4.0*ComplexI*Phik2);
                            }
                            else{
                                std::cerr << "# NO K-INTEGRATION SCHEME DENOTED!!!" << std::endl;
                                exit(0);
                            }
                            
                            // INCREMENT COUNT //
                            NcountREF++;
                        }
                        
                    } // END IF kY STATEMENT //
                } // END kY LOOP //
            } // END IF kX STATEMENT //
        } // END kX LOOP //
        
        std::cerr << "# REFERENCE BIN CALCULATION COMPLETE " << std::endl;

        // LOOP THROUGH MOMENTA OF INTEREST //
        for(INT nk=0; nk<=kMaxIndex; nk++){

            // MOMENTUM BUFFER -- GeV //
            DOUBLE K=kMin+dkStep*nk;

            std::cerr << "# K=" << K << std::endl;

            // BUFFER FOR ANGULAR INTEGRATION VARIABLES //
            //DOUBLE SI=0.0;
            //DOUBLE ASI=0.0;
            DOUBLE v0=0.0;

            COMPLEX v1={0.0,0.0};
            COMPLEX v2={0.0,0.0};
            COMPLEX v3={0.0,0.0};
            COMPLEX v4={0.0,0.0};

            // BIN COUNTER //
            INT Ncount=0;

            // LOOP OVER ALL LATTICE MOMENTA FOR ANGULAR INTEGRATION -- RESTRICTING FOR CERTAIN KEY BANDS //
            for(INT k2XIndex=0;k2XIndex<Lattice::N[0];k2XIndex++){
                if((k2XIndex<Lattice::N[0]/4+1)||(k2XIndex>3*Lattice::N[0]/4-1)){
                    for(INT k2YIndex=0;k2YIndex<Lattice::N[1];k2YIndex++){
                        if((k2YIndex<Lattice::N[1]/4+1)||(k2YIndex>3*Lattice::N[0]/4-1)){

                            // PHYSICAL MOMENTUM VALUES -- GeV //
                            DOUBLE k2XValue=IndexTok(k2XIndex);
                            DOUBLE k2YValue=IndexTok(k2YIndex);

                            // DEFINE k^2 AND |k| -- GeV^2, GeV //
                            DOUBLE k2Sqr=IndexTokSqr(k2XIndex,k2YIndex);
                            DOUBLE k2Abs=sqrt(k2Sqr);
                            // BIN BY MOMENTUM SPACING //
                            
                            // SINGLE INCLUSIVE BUFFER //
                            COMPLEX SIloc=COMPLEX(0.0);
                            COMPLEX AsSIloc=COMPLEX(0.0,0.0);

                            // DEFINE AZIMUTHAL ANGLE -- DIMLESS //
                            DOUBLE Phik2=atan2(k2YValue,k2XValue);
 
                            // BIN BY MOMENTUM SPACING -- GeV //
                            if((k2Abs-(K-0.5*dkBin))*(k2Abs-(K+0.5*dkBin))<0.0){
                                
                                SIloc=EvenSingleInclusive(k2XIndex,k2YIndex);
                                
                                // ASSIGN IF DESIGNATED //
                                if(AS_FLAG==1){
                                    AsSIloc=OddSingleInclusive(k2XIndex,k2YIndex);
                                }
                                
                                if(kINT_FLAG==0){
                                    v0+=real(k2Abs*dkBin*SIloc);
                                    v1+=k2Abs*dkBin*AsSIloc*exp(1.0*ComplexI*Phik2);
                                    v2+=k2Abs*dkBin*SIloc*exp(2.0*ComplexI*Phik2);
                                    v3+=k2Abs*dkBin*AsSIloc*exp(3.0*ComplexI*Phik2);
                                    v4+=k2Abs*dkBin*SIloc*exp(4.0*ComplexI*Phik2);
                                }
                                else if(kINT_FLAG==1){
                                    v0+=real(k2Abs*SIloc);
                                    v1+=k2Abs*AsSIloc*exp(1.0*ComplexI*Phik2);
                                    v2+=k2Abs*SIloc*exp(2.0*ComplexI*Phik2);
                                    v3+=k2Abs*AsSIloc*exp(3.0*ComplexI*Phik2);
                                    v4+=k2Abs*SIloc*exp(4.0*ComplexI*Phik2);
                                }
                                else if(kINT_FLAG==2){
                                    v0+=real(SIloc);
                                    v1+=AsSIloc*exp(1.0*ComplexI*Phik2);
                                    v2+=SIloc*exp(2.0*ComplexI*Phik2);
                                    v3+=AsSIloc*exp(3.0*ComplexI*Phik2);
                                    v4+=SIloc*exp(4.0*ComplexI*Phik2);
                                }
                                else{
                                    std::cerr << "# NO K-INTEGRATION SCHEME DENOTED!!!" << std::endl;
                                    exit(0);
                                }
                                
                                // INCREMENT COUNT //
                                Ncount++;

                            } // END BIN //
                            
                        } // END IF kY STATEMENT //
                    } // END kY LOOP //
                } // END IF kX STATEMENT //
            } // END kX LOOP //
            

            // OUTPUT //
            // K--0 << v0--1 << real(v1)--2 << imag(v1)--3 << real(v2)--4 << imag(v2)--5 << real(v3)--6 << imag(v3)--7 << real(v4)--8 << imag(v4)--9 << v0REF--10 << real(v1REF)--11 << imag(v1REF)--12 << real(v2REF)--13 << imag(v2REF)--14 << real(v3REF)--15 << imag(v3REF)--16  << real(v4REF)--17 << imag(v4REF)--18 << Ncount--19 << NcountREF--20 << std::endl;

            VNOutStream << K << " " << v0 << " " << real(v1) << " " << imag(v1) << " " << real(v2) << " " << imag(v2) << " " << real(v3) << " " << imag(v3)  << " " << real(v4) << " " << imag(v4) << " " << v0REF << " " << real(v1REF) << " " << imag(v1REF) << " " << real(v2REF) << " " << imag(v2REF) << " " << real(v3REF) << " " << imag(v3REF)  << " " << real(v4REF) << " " << imag(v4REF) << " " << Ncount << " " << NcountREF << std::endl;


        }

        // CLOSE OUTSTREAM //
        VNOutStream.close();
        
        // COMMANDLINE OUTPUT //
        //std::cerr << "# CALCULATING MULTIPLICITY" << std::endl;

        // CREATE OUTPUT STREAM FOR MULTIPLICITY //
        std::ofstream MultOutStream; MultOutStream.open(StringManipulation::StringCast(IO::OutputDirectory,"MultiplicityID",MY_MPI_RNG_SEED,".txt").c_str());

        // DEFINE HISTOGRAM FOR SINGLE INCLUSIVE OF |p| //
        DOUBLE MAXP=2.0*sqrt(2.0)*std::pow(1.0/(Lattice::a[0]*Lattice::a[1]),1.0/2.0)*invfmtoGeV;

        INT NumberOfBinsP=MAXP*32;

        std::cerr << "# MAXP=" << MAXP << " #NumberOfBinsP " << NumberOfBinsP << std::endl;
        Histogram *DistributionHistogram=new Histogram(0.0,MAXP,NumberOfBinsP);

        // SET SINGLE INCLUSIVE TO FOURIER SCALAR FIELD FOR MULTIPLICITY //
        //#pragma omp for collapse(2)
        for(INT kXIndex=0;kXIndex<Lattice::N[0];kXIndex++){
            for(INT kYIndex=0;kYIndex<Lattice::N[1];kYIndex++){

                // DEFINE k^2 -- GeV^2 //
                DOUBLE kSqr=IndexTokSqr(kXIndex,kYIndex);

                if(kSqr!=0){
                
                    DOUBLE kAbs=sqrt(kSqr);

                    // SINGLE INCLUSIVE BUFFER //
                    // Omega^2/((2\pi)^3 k^2) //
                    COMPLEX SI=EvenSingleInclusive(kXIndex,kYIndex);

                    // SET FOURIER SPACE VARIABLE //
                    FourierSpace::Scalar->SetP(kXIndex,kYIndex,0,SI);
                    
                    // UPDATE HISTOGRAM //
                    DistributionHistogram->Count(kAbs,real(SI));
                }

                // SET ZERO POINT TO ZERO //
                FourierSpace::Scalar->SetP(0,0,0,0);

            }
        }
        
        // SET OUTPUT FILE FOR HISTOGRAMMED SINGLE INCLUSIVE //
        std::string OutputFileN=StringManipulation::StringCast(IO::OutputDirectory,"SIDistribution","ID",MY_MPI_RNG_SEED,".txt");
        // SET OUTPUT HEADER //
        std::string HeaderMessageN=StringManipulation::StringCast("# kAbs ","dNd2p");
        // OUTPUT SI DISTRIBUTION //
        DistributionHistogram->Output(HeaderMessageN,OutputFileN);
        // CLEAN-UP //
        delete DistributionHistogram;

        // FOURIER TRANSFORM INTEGRATED SI //
        FourierSpace::Scalar->ExecutePtoX();
        
        // (hbar c)^2/(GeV^2 fm^2) -- 1/Ns^2 NEED BY FFTW //
        DOUBLE LocalMultiplicity=real(FourierSpace::Scalar->GetX(0,0,0))*invfmtoGeV*invfmtoGeV/DOUBLE(Lattice::N[0]*Lattice::N[1]*Lattice::a[0]*Lattice::a[1]);

        // OUTPUT \int d^2p exp(0) dN/d^2p = MULT //
        MultOutStream << LocalMultiplicity << " " << bImpact << std::endl;

        std::cerr << "## MULTIPLICTY=" << LocalMultiplicity << " bImpact=" << bImpact << std::endl;

        // CLOSE OUTPUT //
        MultOutStream.close();

        // COMMANDLINE OUTPUT //
        //std::cerr << "# FINISHED CALCULATING MULTIPLICITY" << std::endl;

    }

    void ComputeSingleParticleDistribtion(){

        std::cerr << "## CONSTRUCTING OMEGAS" << std::endl;

        // DETERMINE OMEGA_S AND OMEGA_A //
        ConstructOmegas(OmegaS::O,OmegaA::O);

        std::cerr << "## FINISHED CONSTRUCTING OMEGAS" << std::endl;

        std::cerr << "## CONSTRUCTING SINGLE INCLUSIVE DISTRIBUTION" << std::endl;

        // COMPUTE FOURIER TRANSFORM OF OMEGAS AND DETERMINE SINGLE PARTICLE SPECTRA AND STORE TO FILE//
        DetermineSingleParticleDistribution(OmegaS::O,OmegaA::O);

        std::cerr << "## FINISHED CONSTRUCTING SINGLE INCLUSIVE DISTRIBUTION" << std::endl;

    }

}


#endif
