N="800"
a="0.01"
NRap="100"
mreg="0.1"
oDIR="TEST"
AS="0"
mkdir -p ${oDIR}
nconfs="1"
IF="input_EXAMPLE"

export OMP_NUM_THREADS=1

mpirun -np 1 ./Simulation.exe -N ${N} -a ${a} -NRap ${NRap} -RegMass ${mreg} -if ${IF} -AS ${AS} -o ${oDIR} -nconfs ${nconfs}
